<?php

declare(strict_types=1);

namespace StockAPI\V1\Search\Handler;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;
use StockAPI\V1\Search\Handler\SearchStockHandler;
use StockDomain\Repository\SearchStockRepository\SearchStockRepository;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

use function get_class;

class SearchStockHandlerFactory
{
    public function __invoke(ContainerInterface $container) : RequestHandlerInterface
    {
        return new SearchStockHandler($container->get(SearchStockRepository::class));
    }
}
