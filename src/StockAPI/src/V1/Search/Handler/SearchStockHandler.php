<?php
declare(strict_types=1);

namespace StockAPI\V1\Search\Handler;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use StockDomain\Repository\SearchStockRepository\SearchStockRepository;
use StockDomain\SearchCondition\AbstractSearchCondition;
use StockDomain\SearchCondition\SearchConditionAggregation;
use Zend\Diactoros\Response\JsonResponse;

class SearchStockHandler implements RequestHandlerInterface
{
    /** @var SearchStockRepository $searchStockRepository */
    private $searchStockRepository;

    /**
     * SearchStockHandler constructor.
     * @param SearchStockRepository $searchStockRepository
     */
    public function __construct(
        SearchStockRepository $searchStockRepository
    ) {
        $this->searchStockRepository = $searchStockRepository;
    }

    /**
     * @TODO FIX conditions if empty
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $conditions = new SearchConditionAggregation($request->getAttribute(AbstractSearchCondition::WHERE_CONDITIONS));

        return new JsonResponse($this->searchStockRepository->search($conditions));
    }
}
