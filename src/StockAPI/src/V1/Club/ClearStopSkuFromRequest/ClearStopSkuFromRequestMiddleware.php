<?php
namespace StockAPI\V1\Club\ClearStopSkuFromRequest;

use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use StockAPI\V1\Stock\CreateValueObjectsInRequestMiddleware;
use StockDomain\Repository\Club\GetStopListBySku\GetStopListBySku;
use StockDomain\ValueObjects\Sku\SkuVO;
use StockDomain\ValueObjects\StockType\StockTypeVO;
use Zend\Diactoros\Response\JsonResponse;

class ClearStopSkuFromRequestMiddleware implements MiddlewareInterface
{

    /** @var GetStopListBySku $getStopListBySku */
    private $getStopListBySku;

    /** @var array $valueObjects */
    private $valueObjects;

    public function __construct(GetStopListBySku $getStopListBySku)
    {
        $this->getStopListBySku = $getStopListBySku;
    }

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws OnlyStopSkuRequestException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            $this->valueObjects = $request->getAttribute(CreateValueObjectsInRequestMiddleware::VALUE_OBJECTS);

            $this->clearValueObjectsByStopSku(
                $this->getStopListBySku->__invoke(
                    $this->getSkuCollection()
                )
            );
            $this->breakIfEmptyMessage();
            return $handler->handle(
                $request->withAttribute(
                    CreateValueObjectsInRequestMiddleware::VALUE_OBJECTS,
                    $this->valueObjects
                )
            );
        } catch (OnlyStopSkuRequestException $exception) {
            return new JsonResponse(
                [],
                StatusCodeInterface::STATUS_NO_CONTENT
            );
        }
    }

    /**
     * @return array
     */
    private function getSkuCollection() : array
    {
        $skuCollection = [];

        foreach ($this->valueObjects as $valueObject) {
            $skuCollection[] = $valueObject[SkuVO::NAME]->__tostring();
        }

        return $skuCollection;
    }

    /**
     * @param array $stopList
     */
    private function clearValueObjectsByStopSku(array $stopList)
    {
        foreach ($this->valueObjects as $key => $valueObject) {
            if (!$valueObject[StockTypeVO::NAME]->isVirtualClub()) {
                continue;
            }
            /** @var SkuVO $sku */
            foreach ($stopList as $item) {
                if ($item[SkuVO::NAME]->equals($valueObject[SkuVO::NAME])) {
                    unset($this->valueObjects[$key]);
                }
            }
        }
    }

    /**
     * @throws OnlyStopSkuRequestException
     */
    private function breakIfEmptyMessage()
    {
        if (empty($this->valueObjects)) {
            throw new OnlyStopSkuRequestException();
        }
    }
}
