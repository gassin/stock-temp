<?php
namespace StockAPI\V1\Club\ClearStopSkuFromRequest;

use Exception;

class OnlyStopSkuRequestException extends Exception
{
    protected $message = 'Request contains only SKU which are stopped';
}
