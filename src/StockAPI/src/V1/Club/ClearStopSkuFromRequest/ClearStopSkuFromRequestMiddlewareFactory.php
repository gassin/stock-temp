<?php
namespace StockAPI\V1\Club\ClearStopSkuFromRequest;

use Interop\Container\ContainerInterface;
use StockDomain\Repository\Club\GetStopListBySku\GetStopListBySku;
use Zend\ServiceManager\Factory\FactoryInterface;

class ClearStopSkuFromRequestMiddlewareFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return ClearStopSkuFromRequestMiddleware
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new ClearStopSkuFromRequestMiddleware(
            $container->get(GetStopListBySku::class)
        );
    }
}
