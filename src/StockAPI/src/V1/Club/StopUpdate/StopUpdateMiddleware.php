<?php
namespace StockAPI\V1\Club\StopUpdate;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use StockDomain\Repository\Club\StopUpdate\StopUpdate;
use StockDomain\ValueObjects\Sku\SkuVO;
use StockDomain\ValueObjects\User\UserVO;

class StopUpdateMiddleware implements MiddlewareInterface
{

    /** @var StopUpdate $stopUpdateRepository */
    private $stopUpdateRepository;

    public function __construct(StopUpdate $stopUpdateRepository)
    {
        $this->stopUpdateRepository = $stopUpdateRepository;
    }

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws \Doctrine\DBAL\DBALException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $this->stopUpdateRepository->__invoke(
            $request->getAttribute(SkuVO::NAME),
            $request->getAttribute(UserVO::NAME)
        );

        return $handler->handle($request);
    }
}
