<?php
namespace StockAPI\V1\Club\StopUpdate\ValidateSource;

use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use StockDomain\ValueObjects\Source\SourceVO;
use Zend\Diactoros\Response\JsonResponse;

class ValidateSource implements MiddlewareInterface
{

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        /** @var SourceVO $source */
        $source = $request->getAttribute(SourceVO::NAME);
        if (!$source->isClub()) {
            return new JsonResponse([], StatusCodeInterface::STATUS_FORBIDDEN);
        }

        return $handler->handle($request);
    }
}
