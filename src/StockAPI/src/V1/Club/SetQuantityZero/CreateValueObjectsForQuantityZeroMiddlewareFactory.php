<?php
namespace StockAPI\V1\Club\SetQuantityZero;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use StockDomain\Repository\Club\GetStockAttributesBySku\GetStockAttributesBySku;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class CreateValueObjectsForQuantityZeroMiddlewareFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return CreateValueObjectsForQuantityZeroMiddleware
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new CreateValueObjectsForQuantityZeroMiddleware(
            $container->get(GetStockAttributesBySku::class)
        );
    }
}
