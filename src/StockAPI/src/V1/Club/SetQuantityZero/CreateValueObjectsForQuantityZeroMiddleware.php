<?php
declare(strict_types = 1);

namespace StockAPI\V1\Club\SetQuantityZero;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use StockAPI\V1\Stock\CreateValueObjectsInRequestMiddleware;
use StockDomain\Repository\Club\GetStockAttributesBySku\GetStockAttributesBySku;
use StockDomain\ValueObjects\Location\LocationVO;
use StockDomain\ValueObjects\Quality\QualityVO;
use StockDomain\ValueObjects\Quantity\QuantityVO;
use StockDomain\ValueObjects\Sku\SkuVO;
use StockDomain\ValueObjects\StockType\StockTypeVO;
use Exception;

class CreateValueObjectsForQuantityZeroMiddleware implements MiddlewareInterface
{
    /** @var GetStockAttributesBySku $getCurrentStockBySku */
    private $getStockAttributesBySku;

    public function __construct(GetStockAttributesBySku $getStockAttributesBySku)
    {
        $this->getStockAttributesBySku = $getStockAttributesBySku;
    }

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $result = $this->createValueObjectsForNewSku(
            $request,
            $this->addQuantityToResult(
                $this->getStockAttributesBySku->__invoke(
                    $request->getAttribute(SkuVO::NAME)
                )
            )
        );
        return $handler->handle(
            $request->withAttribute(CreateValueObjectsInRequestMiddleware::VALUE_OBJECTS, $result)
        );
    }

    /**
     * @param array $currentStockAttributes
     * @return array
     */
    private function addQuantityToResult(array $currentStockAttributes) : array
    {
        foreach ($currentStockAttributes as &$currentStockAttribute) {
            $currentStockAttribute[QuantityVO::NAME] = new QuantityVO(0);
        }

        return $currentStockAttributes;
    }

    /**
     * @param ServerRequestInterface $request
     * @param array $result
     * @return array
     * @throws \Exception
     */
    private function createValueObjectsForNewSku(ServerRequestInterface $request, array $result) : array
    {
        $uniqueSku = $this->getUniqueSku($result);

        /** @var SkuVO $sku */
        foreach ($request->getAttribute(SkuVO::NAME) as $sku) {
            if (!in_array($sku->__toString(), $uniqueSku)) {
                try {
                    $valueObjects[SkuVO::NAME] = $sku;
                    $valueObjects[LocationVO::NAME] = LocationVO::create(null);
                    $valueObjects[QuantityVO::NAME] = QuantityVO::create(0);
                    $valueObjects[QualityVO::NAME] = QualityVO::create(QualityVO::QUALITY_NORMAL);
                    $valueObjects[StockTypeVO::NAME] = StockTypeVO::create(StockTypeVO::VIRTUAL_CLUB);
                    $result[] = $valueObjects;
                    unset($valueObjects);
                } catch (Exception $exception) {
                    throw $exception;
                }
            }
        }
        return $result;
    }

    /**
     * @param array $result
     * @return array
     */
    private function getUniqueSku(array $result) : array
    {
        $uniqueSku = [];
        foreach ($result as $item) {
            $uniqueSku[] = $item[SkuVO::NAME]->__toString();
        }
        return $uniqueSku;
    }
}
