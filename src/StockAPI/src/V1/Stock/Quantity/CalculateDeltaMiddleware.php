<?php
namespace StockAPI\V1\Stock\Quantity;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use StockAPI\V1\Stock\CreateValueObjectsInRequestMiddleware;
use StockDomain\Repository\StockQuantityRepository\StockQuantity;
use StockDomain\Repository\StockQuantityRepository\WhereConditionAggregation;
use StockDomain\ValueObjects\Location\LocationVO;
use StockDomain\ValueObjects\Quality\QualityVO;
use StockDomain\ValueObjects\Quantity\QuantityVO;
use StockDomain\ValueObjects\Quantity\QuantityZeroException;
use StockDomain\ValueObjects\Sku\SkuVO;
use StockDomain\ValueObjects\StockType\StockTypeVO;

class CalculateDeltaMiddleware implements MiddlewareInterface
{

    /** @var StockQuantity $stockQuantityRepository */
    private $stockQuantityRepository;

    public function __construct(StockQuantity $stockQuantityRepository)
    {
        $this->stockQuantityRepository = $stockQuantityRepository;
    }

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws QuantityZeroException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $currentStock = $this->stockQuantityRepository->__invoke(
            WhereConditionAggregation::generateByRequest(
                CreateValueObjectsInRequestMiddleware::getValueObjects($request)
            )
        );
        return $handler->handle($request->withAttribute(
            CreateValueObjectsInRequestMiddleware::VALUE_OBJECTS,
            $this->getNewStockResultWithCalculatedData($request, $currentStock)
        ));
    }

    /**
     * @param ServerRequestInterface $request
     * @param array $currentStock
     * @return array
     * @throws QuantityZeroException
     */
    private function getNewStockResultWithCalculatedData(
        ServerRequestInterface $request,
        array $currentStock
    ) : array {
        $result = [];
        foreach (CreateValueObjectsInRequestMiddleware::getValueObjects($request) as $newStockItem) {
            $newItem = [];
            foreach ($currentStock as $currentStockItem) {
                if ($this->compare($newStockItem, $currentStockItem)) {
                    $newItem = [
                        QuantityVO::NAME => $this->getNewQuantityVO(
                            $currentStockItem[QuantityVO::NAME],
                            $newStockItem[QuantityVO::NAME]
                        ),
                        SkuVO::NAME => $newStockItem[SkuVO::NAME],
                        QualityVO::NAME => $newStockItem[QualityVO::NAME],
                        StockTypeVO::NAME => $newStockItem[StockTypeVO::NAME],
                        LocationVO::NAME => $newStockItem[LocationVO::NAME]
                    ];
                }
            }
            if (!$newItem) {
                $newItem = $newStockItem;
            }
            $result[] = $newItem;
        }
        return $result;
    }

    /**
     * @param QuantityVO $currentQuantity
     * @param QuantityVO $newQuantity
     * @return QuantityVO
     * @throws QuantityZeroException
     * @todo CHECK THIS <=0 condition
     */
    private function getNewQuantityVO(QuantityVO $currentQuantity, QuantityVO $newQuantity) : QuantityVO
    {
        if ($newQuantity->get() <= 0) {
            return new QuantityVO((-1)*$currentQuantity->get());
        } else {
            $deltaValue = $newQuantity->get() - $currentQuantity->get();
            $deltaValue <= 0 ? 0 : $deltaValue;
            return new QuantityVO($deltaValue);
        }
    }

    /**
     * @param $requestArray
     * @param $quantitiesArray
     * @return bool
     */
    private function compare($requestArray, $quantitiesArray)
    {
        return $requestArray[SkuVO::NAME]->equals($quantitiesArray[SkuVO::NAME]) &&
            $requestArray[QualityVO::NAME]->equals($quantitiesArray[QualityVO::NAME]) &&
            $requestArray[StockTypeVO::NAME]->equals($quantitiesArray[StockTypeVO::NAME]) &&
            $requestArray[LocationVO::NAME]->equals($quantitiesArray[LocationVO::NAME]);
    }
}
