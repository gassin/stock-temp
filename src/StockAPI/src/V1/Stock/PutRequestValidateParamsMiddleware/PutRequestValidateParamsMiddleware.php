<?php
namespace StockAPI\V1\Stock\PutRequestValidateParamsMiddleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Exception;
use StockDomain\ValueObjects\Quality\QualityVO;
use StockDomain\ValueObjects\Quantity\QuantityVO;
use StockDomain\ValueObjects\Sku\SkuVO;
use StockDomain\ValueObjects\StockType\StockTypeVO;

class PutRequestValidateParamsMiddleware implements MiddlewareInterface
{
    /** @var ServerRequestInterface $request */
    private $request;

    /** @var array $requiredInputParams */
    private $requiredInputParams = [
        SkuVO::NAME,
        QualityVO::NAME,
        QuantityVO::NAME,
        StockTypeVO::NAME
    ];

    /** @var array $requestParams */
    private $requestParams = [];

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws Exception
     */
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ) : ResponseInterface {
        $this->request = $request;
        try {
            $this->requestParams = $request->getParsedBody();
            $this->validateRequestParams();
            return $handler->handle($request);
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @throws Exception
     */
    private function validateRequestParams()
    {
        $emptyParams = [];
        try {
            foreach ($this->requestParams as $subRequest) {
                foreach ($this->requiredInputParams as $requiredInputParam) {
                    if (!isset($subRequest[$requiredInputParam])) {
                        $emptyParams [] = $requiredInputParam;
                    }
                }
            }

            array_unique($emptyParams);

            if ($emptyParams) {
                throw new RequiredParamNotSetException(implode(',', $emptyParams));
            }
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
