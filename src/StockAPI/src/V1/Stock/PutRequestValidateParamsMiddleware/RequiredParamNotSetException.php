<?php
namespace StockAPI\V1\Stock\PutRequestValidateParamsMiddleware;

class RequiredParamNotSetException extends \Exception
{
    const CODE = 100;
}
