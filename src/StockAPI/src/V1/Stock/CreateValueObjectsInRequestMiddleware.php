<?php
namespace StockAPI\V1\Stock;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use StockDomain\ValueObjects\Location\LocationVO;
use StockDomain\ValueObjects\Quality\QualityVO;
use StockDomain\ValueObjects\Quantity\QuantityVO;
use StockDomain\ValueObjects\Sku\SkuVO;
use StockDomain\ValueObjects\StockType\StockTypeVO;
use Exception;

class CreateValueObjectsInRequestMiddleware implements MiddlewareInterface
{

    const VALUE_OBJECTS = 'valueObjects';

    private $requestParams;

    private $valueObjects = [];

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $valueObjects = [];
        $this->requestParams = $request->getParsedBody();
        foreach ($this->requestParams as $requestParam) {
            try {
                $valueObjects[SkuVO::NAME] = new SkuVO(
                    $requestParam[SkuVO::NAME]
                );
                $valueObjects[LocationVO::NAME] = new LocationVO(
                    isset($requestParam[LocationVO::NAME]) ? $requestParam[LocationVO::NAME] : null
                );
                $valueObjects[QuantityVO::NAME] = new QuantityVO(
                    $requestParam[QuantityVO::NAME]
                );
                $valueObjects[QualityVO::NAME] = new QualityVO(
                    $requestParam[QualityVO::NAME]
                );
                $valueObjects[StockTypeVO::NAME] = new StockTypeVO(
                    $requestParam[StockTypeVO::NAME]
                );
                $this->valueObjects[] = $valueObjects;
                unset($valueObjects);
            } catch (\Exception $exception) {
                throw $exception;
            }
        }

        return $handler->handle($request->withAttribute(self::VALUE_OBJECTS, $this->valueObjects));
    }

    /**
     * @param ServerRequestInterface $request
     * @return mixed
     */
    public static function getValueObjects(ServerRequestInterface $request) : array
    {
        return $request->getAttribute(CreateValueObjectsInRequestMiddleware::VALUE_OBJECTS);
    }
}
