<?php
namespace StockAPI\V1\Auth\Token;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use StockDomain\ValueObjects\Source\SourceVO;

class ValidateTokenMiddleware implements MiddlewareInterface
{
    const TOKEN_HEADER_TITLE = 'auth-token';

    private $tokens;

    /**
     * ValidateTokenMiddleware constructor.
     * @param array $tokens
     */
    public function __construct(array $tokens)
    {
        $this->tokens = $tokens;
    }

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws InvalidTokenException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $token = $request->getHeaderLine(self::TOKEN_HEADER_TITLE);
        if (!$source = array_search($token, $this->tokens)) {
            throw new InvalidTokenException();
        }

        return $handler->handle($request->withAttribute(SourceVO::NAME, new SourceVO($source)));
    }
}
