<?php
namespace StockAPI\V1\Validation\SourceStockType;

use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use StockAPI\V1\Stock\CreateValueObjectsInRequestMiddleware;
use StockDomain\Service\ValidateSourceStockType\ValidateSourceStockType;
use StockDomain\ValueObjects\Source\SourceVO;
use StockDomain\ValueObjects\StockType\StockTypeVO;
use Zend\Diactoros\Response\JsonResponse;

class SourceStockTypeValidate implements MiddlewareInterface
{
    /** @var ValidateSourceStockType $validateSourceStockTypeService */
    private $validateSourceStockTypeService;

    /**
     * SourceStockTypeValidate constructor.
     * @param $validateSourceStockTypeService
     */
    public function __construct($validateSourceStockTypeService)
    {
        $this->validateSourceStockTypeService = $validateSourceStockTypeService;
    }

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        foreach ($request->getAttribute(CreateValueObjectsInRequestMiddleware::VALUE_OBJECTS) as $valueObject) {
            if (!$this->validateSourceStockTypeService->__invoke(
                $request->getAttribute(SourceVO::NAME),
                $valueObject[StockTypeVO::NAME]
            )) {
                return new JsonResponse([], StatusCodeInterface::STATUS_FORBIDDEN);
            }
        }
        return $handler->handle($request);
    }
}
