<?php
namespace StockAPI\V1\Validation\NegativeQuantity;

use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Exception;
use StockAPI\V1\Validation\NegativeQuantity\NegativeQuantityException;
use StockDomain\ValueObjects\Location\LocationVO;
use StockDomain\ValueObjects\Quality\QualityVO;
use StockDomain\ValueObjects\Quantity\QuantityVO;
use StockDomain\ValueObjects\Sku\SkuVO;
use StockDomain\ValueObjects\StockType\StockTypeVO;
use Zend\Diactoros\Response\JsonResponse;

class NegativeQuantityValidateParamsMiddleware implements MiddlewareInterface
{
    /** @var ServerRequestInterface $request */
    private $request;

    /** @var array $requestParams */
    private $requestParams = [];

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws Exception
     */
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ) : ResponseInterface {
        $this->request = $request;
        try {
            $this->requestParams = $request->getParsedBody();
            $this->validateQuantityNegative();
            return $handler->handle($request);
        } catch (Exception $exception) {
            return new JsonResponse([], StatusCodeInterface::STATUS_BAD_REQUEST);
        }
    }

    /**
     * @throws Exception
     */
    private function validateQuantityNegative()
    {
        try {
            foreach ($this->requestParams as $param) {
                if ($param[QuantityVO::NAME] < 0) {
                    throw new NegativeQuantityException();
                }
            }
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
