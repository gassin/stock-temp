<?php
declare(strict_types = 1);

namespace StockAPI\V1\Validation\NegativeQuantity;

use Exception;

class NegativeQuantityException extends Exception
{

}
