<?php
declare(strict_types=1);

namespace StockAPI;

use StockAPI\V1\Club\ClearStopSkuFromRequest\ClearStopSkuFromRequestMiddleware;
use StockAPI\V1\Club\ClearStopSkuFromRequest\ClearStopSkuFromRequestMiddlewareFactory;
use StockAPI\V1\Club\StopUpdate\StopUpdateMiddleware;
use StockAPI\V1\Club\StopUpdate\StopUpdateMiddlewareFactory;
use StockAPI\V1\Search\Handler\SearchStockHandlerFactory;
use StockAPI\V1\Search\Handler\SearchStockHandler;
use StockAPI\V1\Stock\Quantity\CalculateDeltaMiddleware;
use StockAPI\V1\Stock\Quantity\CalculateDeltaMiddlewareFactory;
use StockAPI\V1\Auth\Token\ValidateTokenMiddleware;
use StockAPI\V1\Auth\Token\ValidateTokenMiddlewareFactory;
use StockAPI\V1\Club\SetQuantityZero\CreateValueObjectsForQuantityZeroMiddleware;
use StockAPI\V1\Club\SetQuantityZero\CreateValueObjectsForQuantityZeroMiddlewareFactory;
use StockAPI\V1\Validation\SourceStockType\SourceStockTypeValidate;
use StockAPI\V1\Validation\SourceStockType\SourceStockTypeValidateFactory;

/**
 * The configuration provider for the StockAPI module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    const PUT_STOCK_METHOD_PATH = '/api/v1/stock';
    const PUT_STOCK_QUANTITY_METHOD_PATH = '/api/v1/stock/quantity';
    const PUT_CLUB_STOP_SKU_UPDATE_METHOD_PATH = '/api/v1/club/stock/stop';
    const POST_SEARCH_STOCK_METHOD_PATH = '/api/v1/stock/search';

    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     */
    public function __invoke() : array
    {
        return [
            'dependencies' => $this->getDependencies(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies() : array
    {
        return [
            'invokables' => [
            ],
            'factories'  => [
                SearchStockHandler::class => SearchStockHandlerFactory::class,
                CalculateDeltaMiddleware::class => CalculateDeltaMiddlewareFactory::class,
                StopUpdateMiddleware::class => StopUpdateMiddlewareFactory::class,
                ValidateTokenMiddleware::class => ValidateTokenMiddlewareFactory::class,
                CreateValueObjectsForQuantityZeroMiddleware::class
                => CreateValueObjectsForQuantityZeroMiddlewareFactory::class,
                ClearStopSkuFromRequestMiddleware::class => ClearStopSkuFromRequestMiddlewareFactory::class,
                SourceStockTypeValidate::class => SourceStockTypeValidateFactory::class
            ]
        ];
    }
}
