<?php
namespace StockClient;

/**
 * The configuration provider for the StockClient module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    const PUT_STOCK_METHOD_PATH = '/api/v1/stock';
    const PUT_STOCK_QUANTITY_METHOD_PATH = '/api/v1/stock/quantity';
    const PUT_CLUB_STOP_SKU_UPDATE_METHOD_PATH = '/api/v1/club/stock/stop';
    const POST_SEARCH_STOCK_METHOD_PATH = '/api/v1/stock/search';

    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     */
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencies(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies()
    {
        return [
            'invokables' => [
            ],
            'factories'  => [
            ],
        ];
    }
}
