<?php
namespace StockClient\StockLog;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use StockClient\AbstractStockClient;
use StockClient\ConfigProvider;

class StockSearchClient extends AbstractStockClient
{
    /**
     * @param array $message
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function __invoke($message = [])
    {
        try {
            return $this->request(
                $message
            );
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param array $message
     * @return ResponseInterface
     * @throws GuzzleException
     */
    protected function request($message = [])
    {
        try {
            return $this->client->request(
                'POST',
                $this->sstEndPoint.ConfigProvider::POST_SEARCH_STOCK_METHOD_PATH,
                [
                    'body' => json_encode($message)
                ]
            );
        } catch (GuzzleException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param array $message
     * @param array $config
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public static function post(
        $message = [],
        $config = []
    ) {
        try {
            $self = new self(self::getClient($config));
            return $self->__invoke($message);
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
