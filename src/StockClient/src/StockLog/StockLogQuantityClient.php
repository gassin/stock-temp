<?php
namespace StockClient\StockLog;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use StockClient\AbstractStockClient;
use StockClient\ConfigProvider;

class StockLogQuantityClient extends AbstractStockClient
{
    /**
     * @param StockLogCollection $stockLogCollection
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function __invoke(
        StockLogCollection $stockLogCollection
    ) {
        try {
            return $this->request(
                $stockLogCollection
            );
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param StockLogCollection $stockLog
     * @return ResponseInterface
     * @throws GuzzleException
     */
    protected function request(StockLogCollection $stockLog)
    {
        try {
            return $this->client->request(
                'PUT',
                $this->sstEndPoint.ConfigProvider::PUT_STOCK_QUANTITY_METHOD_PATH,
                [
                    'json' => $stockLog->jsonSerialize(),
                ]
            );
        } catch (GuzzleException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param StockLogCollection $stockLogCollection
     * @param array $config
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public static function put(
        StockLogCollection $stockLogCollection,
        $config = []
    ) {
        try {
            $self = new self(self::getClient($config));
            return $self->__invoke($stockLogCollection);
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
