<?php
namespace StockClient\StockLog\Club;

use JsonSerializable;

class SkuCollection implements JsonSerializable
{
    private $collection = [];

    public function add(
        $sku
    ) {
        $this->collection['sku'][] = $sku;
    }

    public function get()
    {
        $this->collection;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return $this->collection;
    }

    /**
     * @param array $items
     */
    public function addByItems($items = [])
    {
        foreach ($items as $sku) {
            $this->add(
                $sku
            );
        }
    }

    /**
     * @param array $items
     * @return SkuCollection
     */
    public static function create($items = [])
    {
        $self = new self();
        $self->addByItems($items);
        return $self;
    }
}
