<?php
namespace StockClient\StockLog\Club;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use StockClient\AbstractStockClient;
use StockClient\ConfigProvider;
use StockClient\StockLog\StockLogCollection;

class StockStopSkuUpdateClient extends AbstractStockClient
{
    /**
     * @param SkuCollection $skuCollection
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function __invoke(
        SkuCollection $skuCollection
    ) {
        try {
            return $this->request(
                $skuCollection
            );
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param SkuCollection $skuCollection
     * @return ResponseInterface
     * @throws GuzzleException
     */
    protected function request(SkuCollection $skuCollection)
    {
        try {
            return $this->client->request(
                'PUT',
                $this->sstEndPoint.ConfigProvider::PUT_CLUB_STOP_SKU_UPDATE_METHOD_PATH,
                [
                    'json' => $skuCollection->jsonSerialize(),
                ]
            );
        } catch (GuzzleException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param SkuCollection $skuCollection
     * @param array $config
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public static function put(
        SkuCollection $skuCollection,
        $config = []
    ) {
        try {
            $self = new self(self::getClient($config));
            return $self->__invoke($skuCollection);
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
