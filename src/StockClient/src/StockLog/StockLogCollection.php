<?php
namespace StockClient\StockLog;

use JsonSerializable;

class StockLogCollection implements JsonSerializable
{
    private $collection = [];

    public function add(
        $sku,
        $quantity,
        $quality,
        $stockType,
        $location = null
    ) {
        $this->collection[] = [
            'sku' => $sku,
            'quantity' => $quantity,
            'quality' => $quality,
            'stockType' => $stockType,
            'location' => $location
        ];
    }

    public function get()
    {
        $this->collection;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return $this->collection;
    }

    /**
     * @param array $items
     */
    public function addByItems($items = [])
    {
        foreach ($items as $item) {
            $this->add(
                $item['sku'],
                $item['quantity'],
                $item['quality'],
                $item['stockType'],
                $item['location']
            );
        }
    }

    /**
     * @param array $items
     * @return StockLogCollection
     */
    public static function create($items = [])
    {
        $self = new self();
        $self->addByItems($items);
        return $self;
    }
}
