<?php
namespace StockClient;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Uri;
use Exception;

abstract class AbstractStockClient
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /** @var string $sstEndPoint */
    protected $sstEndPoint;

    /**
     * StockLog constructor.
     * @param ClientInterface|null $client
     * @param string $sstEndPoint
     * @throws Exception
     */
    public function __construct(ClientInterface $client, $sstEndPoint = null)
    {
        $this->client = $client;
        $this->setClientEndPointIfExist($sstEndPoint);
        $this->setEnvEndPointIfExist();

        if (!$this->sstEndPoint) {
            throw new Exception('API Endpoint not SET');
        }
    }

    /**
     * @param null|string $sstEndPoint
     */
    protected function setEndpoint($sstEndPoint = null)
    {
        $this->sstEndPoint = $sstEndPoint;
    }

    protected function setClientEndPointIfExist($sstEndPoint = null)
    {
        if (!$sstEndPoint && isset($this->client->getConfig()['base_uri'])) {
            /** @var Uri $uri */
            $uri = $this->client->getConfig()['base_uri'];
            $this->setEndpoint($uri->__toString());
        }
    }

    protected function setEnvEndPointIfExist()
    {
        if (!$this->sstEndPoint && $sstEndPoint = getenv('SST_ENDPOINT')) {
            $this->setEndpoint($sstEndPoint);
        }
    }

    /**
     * @param array $clientConfig
     * @return Client
     */
    protected static function getClient($clientConfig = [])
    {
        $config = [
            'headers' => [
                'Content-Type' => 'application/json'
            ],
        ];

        if (isset($clientConfig['auth-token'])) {
            $config['headers']['auth-token'] = $clientConfig['auth-token'];
        }

        if (isset($clientConfig['user'])) {
            $config['headers']['user'] = $clientConfig['user'];
        }

        if (isset($clientConfig['headers'])) {
            $config['headers'] = array_merge($clientConfig['headers'], $config['headers']);
        }

        if (isset($clientConfig['endPoint'])) {
            $config['base_uri'] = $clientConfig['endPoint'];
        }

        return new Client(
            $config
        );
    }
}
