<?php

declare(strict_types=1);

namespace StockDomain;

use StockDomain\Entity\Manager\FlushMiddleware\FlushMiddleware;
use StockDomain\Entity\Manager\FlushMiddleware\FlushMiddlewareFactory;
use StockDomain\Entity\Middleware\StockLog\StockLogCreateEntityMiddlewareFactory;
use StockDomain\Entity\Middleware\StockLog\StockLogCreateEntityMiddleware;
use StockDomain\Repository\Club\GetStopListBySku\GetStopListBySku;
use StockDomain\Repository\Club\GetStopListBySku\GetStopListBySkuFactory;
use StockDomain\Repository\Club\StopUpdate\StopUpdate;
use StockDomain\Repository\Club\StopUpdate\StopUpdateFactory;
use StockDomain\Repository\SearchStockRepository\SearchStockRepository;
use StockDomain\Repository\SearchStockRepository\SearchStockRepositoryFactory;
use StockDomain\Repository\StockLogRepository\StockLogCreateRepository;
use StockDomain\Repository\StockLogRepository\StockLogCreateRepositoryFactory;
use StockDomain\Repository\StockQuantityRepository\StockQuantityFactory;
use StockDomain\Repository\StockQuantityRepository\StockQuantity;
use StockDomain\Service\ValidateSourceStockType\ValidateSourceStockType;
use StockDomain\ValueObjects\Quality\QualityMiddleware;
use StockDomain\ValueObjects\Quantity\QuantityMiddleware;
use StockDomain\ValueObjects\Location\LocationMiddleware;
use StockDomain\ValueObjects\StockType\StockTypeMiddleware;
use StockDomain\ValueObjects\Sku\SkuMiddleware;
use Zend\ServiceManager\Factory\InvokableFactory;
use StockDomain\Repository\Club\GetStockAttributesBySku\GetStockAttributesBySku;
use StockDomain\Repository\Club\GetStockAttributesBySku\GetStockAttributesBySkuFactory;

/**
 * The configuration provider for the StockDomain module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     */
    public function __invoke() : array
    {
        return [
            'dependencies' => $this->getDependencies()
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies() : array
    {
        return [
            'invokables' => [
            ],
            'factories'  => [
                FlushMiddleware::class => FlushMiddlewareFactory::class,
                StockLogCreateEntityMiddleware::class => StockLogCreateEntityMiddlewareFactory::class,
                QuantityMiddleware::class => InvokableFactory::class,
                QualityMiddleware::class => InvokableFactory::class,
                SkuMiddleware::class => InvokableFactory::class,
                LocationMiddleware::class => InvokableFactory::class,
                StockTypeMiddleware::class => InvokableFactory::class,
                StockLogCreateRepository::class => StockLogCreateRepositoryFactory::class,
                SearchStockRepository::class => SearchStockRepositoryFactory::class,
                StockQuantity::class => StockQuantityFactory::class,
                StopUpdate::class => StopUpdateFactory::class,
                GetStockAttributesBySku::class => GetStockAttributesBySkuFactory::class,
                GetStopListBySku::class => GetStopListBySkuFactory::class,
                ValidateSourceStockType::class => InvokableFactory::class
            ],
        ];
    }
}
