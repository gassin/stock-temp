<?php
namespace StockDomain\SearchCondition;

interface SearchConditionInterface
{
    public function getWhere(): string;
    public function getParams() : array;
}
