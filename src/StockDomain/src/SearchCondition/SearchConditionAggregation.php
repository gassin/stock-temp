<?php
namespace StockDomain\SearchCondition;

use StockDomain\SearchCondition\AbstractSearchCondition;

class SearchConditionAggregation extends AbstractSearchCondition
{
    public function __construct(array $conditions)
    {
        $where = [];

        /** @var SearchConditionInterface $condition */
        foreach ($conditions as $condition) {
            $this->whereParams = array_merge($this->whereParams, $condition->getParams());
            $where[] = '('.$condition->getWhere().')';
        }
        $this->where = implode(' AND ', $where);
    }
}
