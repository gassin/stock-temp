<?php
namespace StockDomain\SearchCondition;

use StockDomain\SearchCondition\SearchConditionInterface;

class AbstractSearchCondition implements SearchConditionInterface
{

    const WHERE_CONDITIONS = 'where_conditions';

    /** @var string $where */
    protected $where = '';

    /** @var array $whereParams */
    protected $whereParams = [];

    public function getWhere(): string
    {
        return $this->where;
    }

    public function getParams(): array
    {
        return $this->whereParams;
    }
}
