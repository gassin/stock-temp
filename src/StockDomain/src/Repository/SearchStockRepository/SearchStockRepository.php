<?php
namespace StockDomain\Repository\SearchStockRepository;

use Doctrine\ORM\EntityManagerInterface;
use StockDomain\SearchCondition\SearchConditionInterface;
use StockDomain\Entity\StockLog;

class SearchStockRepository
{

    const TABLE_ALIAS = 'stock';
    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    /**
     * SearchStockRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param SearchConditionInterface $searchConditions
     * @return array
     */
    public function search(SearchConditionInterface $searchConditions)
    {
        $queryBuilder = $this->entityManager
            ->createQueryBuilder();
        if ($searchConditions->getWhere()) {
            $queryBuilder->where($searchConditions->getWhere())
                ->setParameters($searchConditions->getParams());
        }

        $result = $queryBuilder->select('SUM(stock.quantity) as quantity')
            ->addSelect('stock.sku')
            ->addSelect('stock.quality')
            ->addSelect('stock.stockType')
            ->addSelect('stock.location')
            ->from(StockLog::class, 'stock')
            ->groupBy('stock.sku, stock.quality, stock.stockType, stock.location')
            ->getQuery()
            ->getResult();

        return $this->transformResultTotalToInt($result);
    }

    /**
     * @param array $result
     * @return array
     */
    private function transformResultTotalToInt(array $result)
    {
        foreach ($result as &$item) {
            $item['quantity'] = (int) $item['quantity'];
        }
        return $result;
    }
}
