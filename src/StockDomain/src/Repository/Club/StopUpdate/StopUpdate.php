<?php
namespace StockDomain\Repository\Club\StopUpdate;

use Doctrine\ORM\EntityManagerInterface;
use StockDomain\Entity\ClubStockStop;
use StockDomain\ValueObjects\Sku\SkuVO;
use StockDomain\ValueObjects\User\UserVO;

class StopUpdate
{

    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    /** @var \Doctrine\DBAL\Connection $connection */
    private $connection;

    /**
     * SearchStockRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->connection = $entityManager->getConnection();
    }

    /**
     * @param array $skus
     * @param UserVO $user
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function __invoke(array $skus, UserVO $user)
    {

        $query = $this->connection->prepare($this->getQuery());

        /** @var SkuVO $sku */
        foreach ($skus as $sku) {
            $query->execute([
                ':sku' => $sku->get(),
                ':user_name' => $user->get()
            ]);
        }
    }

    private function getQuery()
    {
        return "INSERT INTO " . ClubStockStop::TABLE_NAME . " (sku, user_name) VALUES (:sku, :user_name)
        ON DUPLICATE KEY UPDATE updated_at = NOW(), user_name = :user_name";
    }
}
