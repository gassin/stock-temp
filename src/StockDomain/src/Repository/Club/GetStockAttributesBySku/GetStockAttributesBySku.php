<?php
namespace StockDomain\Repository\Club\GetStockAttributesBySku;

use Doctrine\ORM\EntityManagerInterface;
use StockDomain\Entity\StockLog;
use StockDomain\ValueObjects\StockType\StockTypeVO;

class GetStockAttributesBySku
{
    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    /**
     * SearchStockRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(array $skuCollection)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $result = $queryBuilder->select('stock.sku')
            ->addSelect('stock.quality')
            ->addSelect('stock.stockType')
            ->addSelect('stock.location')
            ->from(StockLog::class, 'stock')
            ->groupBy('stock.sku, stock.quality, stock.stockType, stock.location')
            ->where($queryBuilder->expr()->in('stock.stockType', [StockTypeVO::VIRTUAL_CLUB]))
            ->andWhere($queryBuilder->expr()->in('stock.sku', $skuCollection))
            ->getQuery()
            ->getResult();

        return $result;
    }
}
