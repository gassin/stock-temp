<?php
namespace StockDomain\Repository\Club\GetStockAttributesBySku;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class GetStockAttributesBySkuFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return void
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new GetStockAttributesBySku(
            $container->get('doctrine.entity_manager.orm_default')
        );
    }
}
