<?php
namespace StockDomain\Repository\Club\GetStopListBySku;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class GetStopListBySkuFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new GetStopListBySku(
            $container->get('doctrine.entity_manager.orm_default')
        );
    }
}
