<?php
namespace StockDomain\Repository\Club\GetStopListBySku;

use Doctrine\ORM\EntityManagerInterface;
use StockDomain\Entity\ClubStockStop;

class GetStopListBySku
{
    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    /**
     * SearchStockRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(array $skuCollection)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $result = $queryBuilder->select('clubStockStop.sku')
            ->from(ClubStockStop::class, 'clubStockStop')
            ->andWhere($queryBuilder->expr()->in('clubStockStop.sku', $skuCollection))
            ->getQuery()
            ->getResult();

        return $result;
    }
}
