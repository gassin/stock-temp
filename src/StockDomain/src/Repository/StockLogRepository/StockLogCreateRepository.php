<?php
namespace StockDomain\Repository\StockLogRepository;

use StockDomain\Entity\StockLog;
use StockDomain\ValueObjects\Id\IdVO;
use StockDomain\ValueObjects\Location\LocationVO;
use StockDomain\ValueObjects\Quality\QualityVO;
use StockDomain\ValueObjects\Quantity\QuantityVO;
use StockDomain\ValueObjects\Sku\SkuVO;
use StockDomain\ValueObjects\StockType\StockTypeVO;
use StockDomain\Interfaces\ICreateStockLog;
use Exception;
use StockDomain\ValueObjects\User\UserVO;

class StockLogCreateRepository implements ICreateStockLog
{
    /**
     * @param SkuVO $sku
     * @param QuantityVO $quantity
     * @param QualityVO $quality
     * @param StockTypeVO $stockType
     * @param LocationVO $location
     * @param UserVO $user
     * @return StockLog
     */
    public function create(
        SkuVO $sku,
        QuantityVO $quantity,
        QualityVO $quality,
        StockTypeVO $stockType,
        LocationVO $location,
        UserVO $user
    ) : StockLog {
        return new StockLog(IdVO::createNew(), $sku, $quantity, $quality, $stockType, $location, $user);
    }

    /**
     * @param string $sku
     * @param int $quantity
     * @param int $quality
     * @param string $stockType
     * @param null|string $location
     * @param string|null $user
     * @return StockLog
     * @throws Exception
     */
    public static function createByPrimitives(
        string $sku,
        int $quantity,
        int $quality,
        string $stockType,
        ?string $location,
        ?string $user
    ) : StockLog {

        try {
            $self = new self();
            return $self->create(
                new SkuVO($sku),
                new QuantityVO($quantity),
                new QualityVO($quality),
                new StockTypeVO($stockType),
                new LocationVO($location),
                new UserVO($user)
            );
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
