<?php
namespace StockDomain\Repository\StockLogRepository;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class StockLogCreateRepositoryFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new StockLogCreateRepository(
            //            $container->get('doctrine.entity_manager.orm_default')
        );
    }
}
