<?php
namespace StockDomain\Repository\StockQuantityRepository;

use StockDomain\Interfaces\IGet;
use StockDomain\SearchCondition\AbstractSearchCondition;
use StockDomain\ValueObjects\Location\LocationVO;
use StockDomain\ValueObjects\Quality\QualityVO;
use StockDomain\ValueObjects\Sku\SkuVO;
use StockDomain\ValueObjects\StockType\StockTypeVO;

class WhereCondition extends AbstractSearchCondition
{
    private $conditions = [];

    const ALLOWED_CONDITIONS = [
        QualityVO::NAME,
        SkuVO::NAME,
        LocationVO::NAME,
        StockTypeVO::NAME
    ];

    /**
     * WhereCondition constructor.
     * @param array $conditions
     * @param int $index
     */
    public function __construct(array $conditions, int $index = 0)
    {
        $this->conditions = $conditions;
        $this->generateConditions($index);
    }

    /**
     * @param int $index
     */
    private function generateConditions(int $index)
    {
        $result = [];
        /**
         * @var string $column
         * @var IGet $value
         */
        foreach ($this->conditions as $column => $value) {
            if (!$this->allowedCondition($column)) {
                continue;
            }

            $paramName = ':'.$column.'param'.$index;
            $result[$paramName] = StockQuantity::TABLE_ALIAS. '.' .$column . ' = '. $paramName;
            $this->whereParams[$paramName] = $value->get();
        }

        $this->where = implode(' AND ', $result);
    }

    /**
     * @param $column
     * @return bool
     */
    private function allowedCondition($column) : bool
    {
        return in_array($column, self::ALLOWED_CONDITIONS);
    }
}
