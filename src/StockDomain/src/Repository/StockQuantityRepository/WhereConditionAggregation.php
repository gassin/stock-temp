<?php
namespace StockDomain\Repository\StockQuantityRepository;

use StockDomain\SearchCondition\AbstractSearchCondition;
use StockDomain\SearchCondition\SearchConditionInterface;

class WhereConditionAggregation extends AbstractSearchCondition
{
    /**
     * WhereConditionAggregation constructor.
     * @param array $conditions
     */
    public function __construct(array $conditions)
    {
        $where = [];

        /** @var SearchConditionInterface $condition */
        foreach ($conditions as $condition) {
            $this->whereParams = array_merge($this->whereParams, $condition->getParams());
            $where[] = '('.$condition->getWhere().')';
        }
        $this->where = implode(' OR ', $where);
    }

    /**
     * @param array $requestValueObjects
     * @return WhereConditionAggregation
     */
    public static function generateByRequest(array $requestValueObjects)
    {
        $whereConditions = [];
        if ($requestValueObjects) {
            foreach ($requestValueObjects as $index => $conditions) {
                $whereConditions[] = new WhereCondition($conditions, $index);
            }
        }
        return new self($whereConditions);
    }
}
