<?php
namespace StockDomain\Repository\StockQuantityRepository;

use Doctrine\ORM\EntityManagerInterface;
use StockDomain\Entity\StockLog;
use StockDomain\ValueObjects\Location\LocationVO;
use StockDomain\ValueObjects\Quality\QualityVO;
use StockDomain\ValueObjects\Quantity\QuantityVO;
use JsonSerializable;
use StockDomain\ValueObjects\Sku\SkuVO;
use StockDomain\ValueObjects\StockType\StockTypeVO;

class StockQuantity implements JsonSerializable
{
    const TABLE_ALIAS = 'stock';

    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    /** @var \Doctrine\DBAL\Connection $connection */
    private $connection;

    /** @var array $result */
    private $result;

    /**
     * SearchStockRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->connection = $entityManager->getConnection();
    }

    public function __invoke(WhereConditionAggregation $stockLogWhereAggregation)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder->select('SUM(stock.quantity) as quantity')
            ->addSelect('stock.sku')
            ->addSelect('stock.quality')
            ->addSelect('stock.stockType')
            ->addSelect('stock.location')
            ->from(StockLog::class, 'stock')
            ->groupBy('stock.sku, stock.quality, stock.stockType, stock.location');

        if ($stockLogWhereAggregation->getWhere()) {
            $queryBuilder->where($stockLogWhereAggregation->getWhere())
                ->setParameters($stockLogWhereAggregation->getParams());
        }

        $this->result = $queryBuilder->getQuery()->getResult();
        $this->transformResultTotalToQuantityVO();
        return $this->result;
    }

    /**
     * @param array $result
     * @return array
     * @throws \StockDomain\ValueObjects\Quantity\QuantityZeroException
     */
    private function transformResultTotalToQuantityVO()
    {
        foreach ($this->result as &$item) {
            $item['quantity'] = new QuantityVO($item['quantity']);
        }
        return $this->result;
    }

    public function jsonSerialize() : array
    {
        $result = [];

        /** @var array $item */
        foreach ($this->result as $item) {
            $serialized = [
                QuantityVO::NAME => $item[QuantityVO::NAME]->jsonSerialize(),
                SkuVO::NAME => $item[SkuVO::NAME]->jsonSerialize(),
                QualityVO::NAME => $item[QualityVO::NAME]->jsonSerialize(),
                LocationVO::NAME => $item[LocationVO::NAME]->jsonSerialize(),
                StockTypeVO::NAME => $item[StockTypeVO::NAME]->jsonSerialize(),
            ];
            $result[] = $serialized;
            unset($serialized);
        }
        return $result;
    }
}
