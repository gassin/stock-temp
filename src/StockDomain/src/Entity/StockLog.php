<?php
namespace StockDomain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Persisters\Entity\BasicEntityPersister;
use StockDomain\Interfaces\IChangeStock;
use StockDomain\ValueObjects\Id\IdVO;
use StockDomain\ValueObjects\Location\LocationVO;
use StockDomain\ValueObjects\Quality\QualityVO;
use StockDomain\ValueObjects\Quantity\QuantityVO;
use StockDomain\ValueObjects\Sku\SkuVO;
use StockDomain\ValueObjects\StockType\StockTypeVO;
use JsonSerializable;
use DateTime;
use Exception;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use StockDomain\ValueObjects\User\UserVO;

/**
 * @ORM\Entity
 * @ORM\Entity @HasLifecycleCallbacks
 * @ORM\Table(name="stock_log")
 */
class StockLog implements JsonSerializable
{
    use EntityUuidTrait;

    /**
     * @ORM\Column(name="sku", type="sku")
     * @var SkuVO $sku
     */
    private $sku;

    /**
     * @ORM\Column(name="quantity", type="quantity")
     * @var QualityVO $quantity
     */
    private $quantity;

    /**
     * @ORM\Column(name="quality", type="quality")
     * @var QualityVO $quantity
     */
    private $quality;

    /**
     * @ORM\Column(name="stockType", type="stockType")
     * @var StockTypeVO $stockType
     */
    private $stockType;

    /**
     * @ORM\Column(name="location", type="location", nullable=true)
     * @var LocationVO $location
     */
    private $location;

    /**
     * @ORM\Column(name="user", type="user", nullable=true)
     * @var UserVO $user
     */
    private $user;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * Application constructor.
     * @param IdVO $id
     * @param SkuVO $sku
     * @param QuantityVO $quantity
     * @param QualityVO $quality
     * @param StockTypeVO $stockType
     * @param LocationVO $location
     * @param UserVO $user
     */
    public function __construct(
        IdVO $id,
        SkuVO $sku,
        QuantityVO $quantity,
        QualityVO $quality,
        StockTypeVO $stockType,
        LocationVO $location,
        UserVO $user
    ) {
        $this->id = $id;
        $this->sku = $sku;
        $this->quantity = $quantity;
        $this->quality = $quality;
        $this->stockType = $stockType;
        $this->location = $location;
        $this->user = $user;
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return [
            IdVO::NAME => $this->id->jsonSerialize(),
            SkuVO::NAME => $this->sku->jsonSerialize(),
            QuantityVO::NAME => $this->quantity->jsonSerialize(),
            QualityVO::NAME => $this->quality->jsonSerialize(),
            StockTypeVO::NAME => $this->stockType->jsonSerialize(),
            LocationVO::NAME => $this->location->jsonSerialize(),
            UserVO::NAME => $this->user->jsonSerialize()
        ];
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new DateTime('now');
    }
}
