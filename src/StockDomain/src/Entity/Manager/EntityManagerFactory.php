<?php
namespace StockDomain\Entity\Manager;

use ContainerInteropDoctrine\AbstractFactory;
use ContainerInteropDoctrine\ConnectionFactory;
use ContainerInteropDoctrine\ConfigurationFactory;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;

class EntityManagerFactory extends AbstractFactory
{
    /**
     * {@inheritdoc}
     */
    protected function createWithConfig(ContainerInterface $container, $configKey)
    {
        $config = $this->retrieveConfig($container, $configKey, 'entity_manager');

        $em = EntityManager::create(
            $this->retrieveDependency(
                $container,
                $config['connection'],
                'connection',
                ConnectionFactory::class
            ),
            $this->retrieveDependency(
                $container,
                $config['configuration'],
                'configuration',
                ConfigurationFactory::class
            )
        );

        return $em;
    }

    /**
     * {@inheritdoc}
     */
    protected function getDefaultConfig($configKey)
    {
        return [
            'connection' => $configKey,
            'configuration' => $configKey,
        ];
    }
}
