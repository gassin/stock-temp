<?php
namespace StockDomain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Persisters\Entity\BasicEntityPersister;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use StockDomain\ValueObjects\Sku\SkuVO;
use JsonSerializable;
use DateTime;
use Exception;
use StockDomain\ValueObjects\User\UserVO;

/**
 * @ORM\Entity
 * @ORM\Entity @HasLifecycleCallbacks
 * @ORM\Table(name="club_stock_stop")
 */
class ClubStockStop implements JsonSerializable
{
    const TABLE_NAME = 'club_stock_stop';

    /**
     * @ORM\Id
     * @ORM\Column(name="sku", type="sku")
     * @var SkuVO $sku
     */
    private $sku;

    /**
     * @ORM\Column(name="user_name", type="user")
     * @var UserVO $user
     */
    private $user;

    /**
     * @var DateTime
     * @ORM\Column(name="created_at", type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @var DateTime
     * @ORM\Column(name="updated_at", type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedAt;

    /**
     * Application constructor.
     * @param SkuVO $sku
     * @param UserVO $user
     */
    public function __construct(
        SkuVO $sku,
        UserVO $user
    ) {
        $this->sku = $sku;
        $this->user = $user;
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return [
            SkuVO::NAME => $this->sku->jsonSerialize(),
            UserVO::NAME => $this->user->jsonSerialize()
        ];
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        if (!$this->createdAt) {
            $this->createdAt = new DateTime('now');
        }

        $this->updatedAt = new DateTime('now');
    }
}
