<?php
namespace StockDomain\Entity\Middleware\StockLog;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use StockAPI\V1\Stock\CreateValueObjectsInRequestMiddleware;
use StockDomain\Interfaces\ICreateStockLog;
use StockDomain\Repository\StockLogRepository\StockLogCreateRepository;
use StockDomain\ValueObjects\Location\LocationVO;
use StockDomain\ValueObjects\Quality\QualityVO;
use StockDomain\ValueObjects\Quantity\QuantityVO;
use StockDomain\ValueObjects\Sku\SkuVO;
use StockDomain\ValueObjects\StockType\StockTypeVO;
use StockDomain\ValueObjects\User\UserVO;

final class StockLogCreateEntityMiddleware implements MiddlewareInterface
{
    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    /** @var StockLogCreateRepository $stockLogCreateRepository */
    private $stockLogCreateRepository;

    /**
     * StockLogMiddleware constructor.
     * @param EntityManagerInterface $entityManager
     * @param ICreateStockLog $stockLogCreateRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ICreateStockLog $stockLogCreateRepository
    ) {
        $this->entityManager = $entityManager;
        $this->stockLogCreateRepository = $stockLogCreateRepository;
    }

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        foreach ($request->getAttribute(CreateValueObjectsInRequestMiddleware::VALUE_OBJECTS) as $valueObject) {
            $this->entityManager->persist(
                $this->stockLogCreateRepository->create(
                    $valueObject[SkuVO::NAME],
                    $valueObject[QuantityVO::NAME],
                    $valueObject[QualityVO::NAME],
                    $valueObject[StockTypeVO::NAME],
                    $valueObject[LocationVO::NAME],
                    $request->getAttribute(UserVO::NAME)
                )
            );
        }
        return $handler->handle($request);
    }
}
