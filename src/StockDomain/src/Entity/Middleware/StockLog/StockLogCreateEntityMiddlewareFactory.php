<?php
namespace StockDomain\Entity\Middleware\StockLog;

use Interop\Container\ContainerInterface;
use StockDomain\Repository\StockLogRepository\StockLogCreateRepository;
use Zend\ServiceManager\Factory\FactoryInterface;

class StockLogCreateEntityMiddlewareFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     * @return object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new StockLogCreateEntityMiddleware(
            $container->get('doctrine.entity_manager.orm_default'),
            $container->get(StockLogCreateRepository::class)
        );
    }
}
