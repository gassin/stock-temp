<?php
declare(strict_types = 1);

namespace StockDomain\ValueObjects\Sku;

use Exception;

class SkuIsNotSetException extends Exception
{
    protected $message = SkuVO::NAME.' is not set';
}
