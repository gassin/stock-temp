<?php
namespace StockDomain\ValueObjects\Sku;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use StockDomain\ValueObjects\Sku\SkuVO;

/**
 * SkuType data type
 */
class SkuType extends Type
{
    /**
     * @param array $fieldDeclaration
     * @param AbstractPlatform $platform
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return mixed|\StockDomain\ValueObjects\Sku\SkuVO
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        try {
            return new SkuVO($value);
        } catch (\Exception $exception) {
        }
    }

    /**
     * @param SkuVO $value
     * @param AbstractPlatform $platform
     * @return mixed
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value->__toString();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return SkuVO::NAME;
    }
}
