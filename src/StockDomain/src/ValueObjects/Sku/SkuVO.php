<?php
namespace StockDomain\ValueObjects\Sku;

use Exception;
use JsonSerializable;
use StockDomain\Interfaces\IGet;

class SkuVO implements JsonSerializable, IGet
{
    const NAME = 'sku';
    private $value;

    /**
     * SkuVO constructor.
     * @param string $value
     * @throws InvalidSkuException
     */
    public function __construct(string $value)
    {
        $this->value = $value;
        $this->validate();
    }

    /**
     * @throws InvalidSkuException
     */
    private function validate()
    {
        if (empty(trim($this->value))) {
            throw new InvalidSkuException();
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * @return mixed|string
     */
    public function jsonSerialize()
    {
        return $this->__toString();
    }

    /**
     * @param string $sku
     * @return SkuVO
     * @throws Exception
     */
    public static function create(string $sku) : SkuVO
    {
        try {
            return new self($sku);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param SkuVO $skuVO
     * @return bool
     */
    public function equals(SkuVO $skuVO) : bool
    {
        return $skuVO->__toString() === $this->value;
    }

    /**
     * @return string
     */
    public function get()
    {
        return $this->__toString();
    }
}
