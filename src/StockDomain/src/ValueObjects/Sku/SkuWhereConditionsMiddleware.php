<?php
namespace StockDomain\ValueObjects\Sku;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use StockDomain\SearchCondition\AbstractSearchCondition;
use Exception;

class SkuWhereConditionsMiddleware implements MiddlewareInterface
{
    private $requestParams = [];
    private $searchConditions = [];

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $this->requestParams = $request->getParsedBody();
        $this->processSku();
        $whereConditions = $request->getAttribute(AbstractSearchCondition::WHERE_CONDITIONS) ?? [];

        if ($this->searchConditions) {
            $whereConditions[SkuVO::NAME] = new SkuWhereCondition($this->searchConditions);
        }

        return $handler->handle($request->withAttribute(AbstractSearchCondition::WHERE_CONDITIONS, $whereConditions));
    }


    private function processSku()
    {
        if (!empty($this->requestParams[SkuVO::NAME])) {
            if (is_array($this->requestParams[SkuVO::NAME])) {
                foreach ($this->requestParams[SkuVO::NAME] as $sku) {
                    try {
                        $this->searchConditions[SkuVO::NAME][] = SkuVO::create($sku);
                    } catch (Exception $exception) {
                        throw $exception;
                    }
                }
            }
        }
    }
}
