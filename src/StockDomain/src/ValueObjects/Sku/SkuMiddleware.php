<?php
declare(strict_types = 1);

namespace StockDomain\ValueObjects\Sku;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Exception;

class SkuMiddleware implements MiddlewareInterface
{
    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            $requestParams = $request->getParsedBody();

            if (!isset($requestParams[SkuVO::NAME])) {
                throw new SkuIsNotSetException();
            }

            $skuCollection = [];

            if (is_array($requestParams[SkuVO::NAME])) {
                foreach ($requestParams[SkuVO::NAME] as $sku) {
                    $skuCollection[] = new SkuVO($sku);
                }
            } elseif (is_string($requestParams[SkuVO::NAME])) {
                $skuCollection[] = new SkuVO($requestParams[SkuVO::NAME]);
            }

            return $handler->handle(
                $request->withAttribute(
                    SkuVO::NAME,
                    $skuCollection
                )
            );
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
