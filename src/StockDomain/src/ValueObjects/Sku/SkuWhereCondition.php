<?php
namespace StockDomain\ValueObjects\Sku;

use StockDomain\Repository\SearchStockRepository\SearchStockRepository;
use StockDomain\SearchCondition\AbstractSearchCondition;

class SkuWhereCondition extends AbstractSearchCondition
{

    private $conditions = [];

    public function __construct(array $conditions)
    {
        $this->conditions = $conditions;
        $this->generateSkuConditions();
    }

    private function generateSkuConditions()
    {
        $skus = [];
        if (!empty($this->conditions[SkuVO::NAME])) {
            /**
             * @var int $key
             * @var SkuVO $condition
             */
            foreach ($this->conditions[SkuVO::NAME] as $key => $condition) {
                $paramName = ':'.SkuVO::NAME.'param'.$key;
                $skus[$paramName] = SearchStockRepository::TABLE_ALIAS.'.'.SkuVO::NAME.' like '.$paramName;
                $this->whereParams[$paramName] = '%'.$condition->__toString().'%';
            }
            $this->where = implode(' or ', $skus);
        }
    }
}
