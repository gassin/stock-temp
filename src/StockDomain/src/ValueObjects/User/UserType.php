<?php
namespace StockDomain\ValueObjects\User;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Exception;

/**
 * StockType data type
 */
class UserType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return mixed|UserVO
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        try {
            return new UserVO($value);
        } catch (Exception $exception) {
        }
    }

    /**
     * @param UserVO $value
     * @param AbstractPlatform $platform
     * @return mixed
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value->__toString();
    }

    public function getName()
    {
        return UserVO::NAME;
    }
}
