<?php
namespace StockDomain\ValueObjects\User;

use JsonSerializable;
use StockDomain\Interfaces\IGet;

class UserVO implements JsonSerializable, IGet
{
    const NAME = 'user';

    /** @var string $user */
    private $user = null;

    /**
     * UserVO constructor.
     * @param string|null $user
     */
    public function __construct(?string $user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return (string) $this->user;
    }

    /**
     * @param UserVO $userVO
     * @return bool
     */
    public function equals(UserVO $userVO) : bool
    {
        return $this->user === $userVO->__toString();
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return $this->__toString();
    }

    /**
     * @return string
     */
    public function get()
    {
        return $this->__toString();
    }
}
