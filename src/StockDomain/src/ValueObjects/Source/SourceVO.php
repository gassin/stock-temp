<?php
namespace StockDomain\ValueObjects\Source;

use StockDomain\Interfaces\IGet;
use JsonSerializable;

class SourceVO implements JsonSerializable, IGet
{
    const NAME = 'source';

    const CLUB = 'club';
    const SHOP = 'shop';
    const DELTA = 'delta';

    const SOURCES = [
        self::CLUB,
        self::SHOP,
        self::DELTA,
    ];

    /** @var string $source */
    private $source;

    /**
     * SourceVO constructor.
     * @param string $source
     */
    public function __construct(string $source)
    {
        $this->source = $source;
    }

    /**
     * @param SourceVO $sourceVO
     * @return bool
     */
    public function equals(SourceVO $sourceVO) : bool
    {
        return $this->source === $sourceVO->__toString();
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->source;
    }

    /**
     * @return string
     */
    public function get()
    {
        return $this->__toString();
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return $this->__toString();
    }

    /**
     * @return bool
     */
    public function isClub() : bool
    {
        return $this->source === self::CLUB;
    }

    /**
     * @return bool
     */
    public function isShop() : bool
    {
        return $this->source === self::SHOP;
    }

    /**
     * @return bool
     */
    public function isDelta() : bool
    {
        return $this->source === self::DELTA;
    }

    /**
     * @param string $source
     * @return SourceVO
     */
    public static function create(string $source) : SourceVO
    {
        return new self($source);
    }
}
