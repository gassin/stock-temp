<?php
namespace StockDomain\ValueObjects\Location;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Exception;
use StockDomain\SearchCondition\AbstractSearchCondition;

class LocationWhereConditionsMiddleware implements MiddlewareInterface
{
    private $requestParams = [];
    private $searchConditions = [];

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $this->requestParams = $request->getParsedBody();
        $this->processLocationRequestParams();
        $whereConditions = $request->getAttribute(AbstractSearchCondition::WHERE_CONDITIONS) ?? [];

        if ($this->searchConditions) {
            $whereConditions[LocationVO::NAME] = new LocationWhereCondition($this->searchConditions);
        }

        return $handler->handle($request->withAttribute(AbstractSearchCondition::WHERE_CONDITIONS, $whereConditions));
    }

    private function processLocationRequestParams()
    {
        if (!empty($this->requestParams[LocationVO::NAME])) {
            if (is_array($this->requestParams[LocationVO::NAME])) {
                foreach ($this->requestParams[LocationVO::NAME] as $sku) {
                    try {
                        $this->searchConditions[LocationVO::NAME][] = LocationVO::create($sku);
                    } catch (Exception $exception) {
                        throw $exception;
                    }
                }
            }
        }
    }
}
