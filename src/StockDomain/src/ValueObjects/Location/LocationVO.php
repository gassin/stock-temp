<?php
declare(strict_types = 1);

namespace StockDomain\ValueObjects\Location;

use StockDomain\Interfaces\IGet;
use JsonSerializable;

class LocationVO implements JsonSerializable, IGet
{
    const NAME = 'location';

    const RLC = 1015;
    const HUB = 1016;
    const WHPSTUDIO = 1012;

    private static $availableCodes = [
        self::RLC => 'RLC',
        self::HUB => 'HUB',
        self::WHPSTUDIO => 'WHPSTUDIO'
    ];

    /** @var int|null $location */
    private $location;

    /**
     * LocationVO constructor.
     * @param int|null $location
     * @throws LocationNotFoundException
     */
    public function __construct(?int $location)
    {
        $this->validate($location);
        $this->location = $location;
    }

    /**
     * @param LocationVO $location
     * @return bool
     */
    public function equals(LocationVO $location) : bool
    {
        return $this->location === $location->get();
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return (string) $this->location;
    }

    /**
     * @return int | null
     */
    public function jsonSerialize() : ?int
    {
        return $this->get();
    }

    /**
     * @param int $location
     * @return bool
     * @throws LocationNotFoundException
     */
    private function validate(?int $location):bool
    {
        if (!is_null($location) && empty(self::$availableCodes[$location])) {
            throw new LocationNotFoundException();
        }
        return true;
    }

    /**
     * @return array
     */
    public static function getAvailableLocationCodes()
    {
        return array_keys(self::$availableCodes);
    }

    /**
     * @return int|null
     */
    public function get() :?int
    {
        return $this->location;
    }

    /**
     * @param int $location
     * @return LocationVO
     * @throws LocationNotFoundException
     */
    public static function create(?int $location) : LocationVO
    {
        try {
            return new self($location);
        } catch (LocationNotFoundException $locationNotFoundException) {
            throw $locationNotFoundException;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}
