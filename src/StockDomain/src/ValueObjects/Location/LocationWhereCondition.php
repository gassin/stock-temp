<?php
namespace StockDomain\ValueObjects\Location;

use StockDomain\Repository\SearchStockRepository\SearchStockRepository;
use StockDomain\SearchCondition\AbstractSearchCondition;
use StockDomain\ValueObjects\Location\LocationVO;

class LocationWhereCondition extends AbstractSearchCondition
{

    private $conditions = [];

    public function __construct(array $conditions)
    {
        $this->conditions = $conditions;
        $this->generateLocationConditions();
    }

    private function generateLocationConditions()
    {
        $locations = [];
        if (!empty($this->conditions[LocationVO::NAME])) {
            /**
             * @var int $key
             * @var LocationVO $condition
             */
            foreach ($this->conditions[LocationVO::NAME] as $key => $condition) {
                $paramName = ':'.LocationVO::NAME.'param'.$key;
                $locations[$paramName] = SearchStockRepository::TABLE_ALIAS.'.'.LocationVO::NAME.' = '.$paramName;
                $this->whereParams[$paramName] = $condition->get();
            }
            $this->where = implode(' or ', $locations);
        }
    }
}
