<?php
namespace StockDomain\ValueObjects\Location;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class LocationMiddleware implements MiddlewareInterface
{
    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws LocationNotFoundException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            return $handler->handle(
                $request->withAttribute(
                    LocationVO::NAME,
                    new LocationVO($request->getAttribute(LocationVO::NAME))
                )
            );
        } catch (LocationNotFoundException $locationNotFoundException) {
            throw $locationNotFoundException;
        }
    }
}
