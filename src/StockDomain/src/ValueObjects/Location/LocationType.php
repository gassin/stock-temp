<?php
declare(strict_types = 1);

namespace StockDomain\ValueObjects\Location;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Exception;

/**
 * StockType data type
 */
class LocationType extends Type
{
    /**
     * @param array $fieldDeclaration
     * @param AbstractPlatform $platform
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getIntegerTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return mixed|\StockDomain\ValueObjects\Location\LocationVO
     * @throws Exception
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        try {
            return new LocationVO(
                $this->toLocationValue($value)
            );
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $value
     * @return int|null
     */
    private function toLocationValue($value) : ?int
    {
        if (!is_null($value)) {
            return (int) $value;
        }
        return null;
    }

    /**
     * @param LocationVO $value
     * @param AbstractPlatform $platform
     * @return mixed
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value->get();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return LocationVO::NAME;
    }
}
