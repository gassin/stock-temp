<?php
namespace StockDomain\ValueObjects\Quantity;

use Exception;

class QuantityZeroException extends Exception
{
    protected $message = 'Zero quantity';
}
