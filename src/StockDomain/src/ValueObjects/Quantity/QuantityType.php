<?php
namespace StockDomain\ValueObjects\Quantity;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use StockDomain\ValueObjects\Quantity\QuantityVO;

/**
 * Quantity data type
 */
class QuantityType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getIntegerTypeDeclarationSQL($fieldDeclaration);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        try {
            return new QuantityVO($value);
        } catch (\Exception $exception) {
        }
    }

    /**
     * @param QuantityVO $value
     * @param AbstractPlatform $platform
     * @return mixed
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value->get();
    }

    public function getName()
    {
        return QuantityVO::NAME;
    }
}
