<?php
namespace StockDomain\ValueObjects\Quantity;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class QuantityMiddleware implements MiddlewareInterface
{
    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws QuantityZeroException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            return $handler->handle(
                $request->withAttribute(
                    QuantityVO::NAME,
                    new QuantityVO($request->getAttribute(QuantityVO::NAME))
                )
            );
        } catch (QuantityZeroException $quantityZeroException) {
            throw $quantityZeroException;
        }
    }
}
