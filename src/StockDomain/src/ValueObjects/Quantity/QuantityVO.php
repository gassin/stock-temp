<?php
namespace StockDomain\ValueObjects\Quantity;

use StockDomain\Interfaces\IGet;
use JsonSerializable;

class QuantityVO implements JsonSerializable, IGet
{
    const NAME = 'quantity';

    /** @var int $value */
    private $value;

    /**
     * QuantityVO constructor.
     * @param int $value
     */
    public function __construct(int $value)
    {
        $this->value = (int) $value;
    }

    /**
     * @param $value
     * @return bool
     * @throws QuantityZeroException
     */
    private function validate(int $value): bool
    {
        if ($value === 0) {
            throw new QuantityZeroException();
        }
        return true;
    }

    /**
     * @return int
     */
    public function get(): int
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->value;
    }

    /**
     * @return int|mixed
     */
    public function jsonSerialize()
    {
        return $this->get();
    }

    /**
     * @param QuantityVO $quantity
     * @return bool
     */
    public function equals(QuantityVO $quantity) : bool
    {
        return $this->get() === $quantity->get();
    }

    /**
     * @param int $quantity
     * @return QuantityVO
     */
    public static function create(int $quantity) : QuantityVO
    {
        return new QuantityVO($quantity);
    }
}
