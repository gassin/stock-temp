<?php
namespace StockDomain\ValueObjects\StockType;

class InvalidStockTypeException extends \Exception
{
    protected $message = 'Invalid stock type';
    protected $code = 1100;
}
