<?php
namespace StockDomain\ValueObjects\StockType;

use StockDomain\Interfaces\IGet;
use StockDomain\ValueObjects\Sku\SkuVO;
use Exception;

class StockTypeVO implements \JsonSerializable, IGet
{
    const NAME = 'stockType';

    const VIRTUAL_CLUB = 'virtualClubStock';
    const VIRTUAL_SHOP = 'virtualShopStock';
    const PHYSICAL_FREE = 'physicalFreeStock';

    /** @var array $availableStockTypes */
    private static $availableStockTypes = [
        self::VIRTUAL_CLUB,
        self::VIRTUAL_SHOP,
        self::PHYSICAL_FREE
    ];

    /** @var int $type */
    private $type;

    /**
     * StockTypeVO constructor.
     * @param string $type
     * @throws InvalidStockTypeException
     */
    public function __construct(string $type)
    {
        $this->type = (string)$type;
        $this->validate();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->type;
    }

    /**
     * @return int|mixed|string
     */
    public function jsonSerialize()
    {
        return $this->type;
    }

    /**
     * @return bool
     * @throws InvalidStockTypeException
     */
    private function validate(): bool
    {
        if (!in_array($this->type, self::$availableStockTypes)) {
            throw new InvalidStockTypeException();
        }
        return true;
    }

    /**
     * @return array
     */
    public static function getAvailableTypes(): array
    {
        return self::$availableStockTypes;
    }

    /**
     * @return string
     */
    public function get()
    {
        return $this->__toString();
    }

    /**
     * @param string $sku
     * @return StockTypeVO
     * @throws Exception
     */
    public static function create(string $sku): StockTypeVO
    {
        try {
            return new self($sku);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param StockTypeVO $stockTypeVO
     * @return bool
     */
    public function equals(StockTypeVO $stockTypeVO): bool
    {
        return $stockTypeVO->__toString() === $this->type;
    }

    /**
     * @return bool
     */
    public function isVirtualClub(): bool
    {
        return $this->type === self::VIRTUAL_CLUB;
    }

    /**
     * @return bool
     */
    public function isVirtualShop(): bool
    {
        return $this->type === self::VIRTUAL_SHOP;
    }

    /**
     * @return bool
     */
    public function isPhysical(): bool
    {
        return $this->type === self::PHYSICAL_FREE;
    }
}
