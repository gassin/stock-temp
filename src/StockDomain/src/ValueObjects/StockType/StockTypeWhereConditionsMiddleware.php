<?php
namespace StockDomain\ValueObjects\StockType;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Exception;
use StockDomain\SearchCondition\AbstractSearchCondition;

class StockTypeWhereConditionsMiddleware implements MiddlewareInterface
{

    private $requestParams = [];
    private $searchConditions = [];

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $this->requestParams = $request->getParsedBody();
        $this->processStockType();
        $whereConditions = $request->getAttribute(AbstractSearchCondition::WHERE_CONDITIONS) ?? [];

        if (!empty($this->searchConditions[StockTypeVO::NAME])) {
            $whereConditions[StockTypeVO::NAME] = new StockTypeWhereCondition($this->searchConditions);
        }

        return $handler->handle($request->withAttribute(AbstractSearchCondition::WHERE_CONDITIONS, $whereConditions));
    }

    /**
     * @throws Exception
     */
    private function processStockType()
    {
        if (!empty($this->requestParams[StockTypeVO::NAME])) {
            if (is_array($this->requestParams[StockTypeVO::NAME])) {
                foreach ($this->requestParams[StockTypeVO::NAME] as $sku) {
                    try {
                        $this->searchConditions[StockTypeVO::NAME][] = StockTypeVO::create($sku);
                    } catch (Exception $exception) {
                        throw $exception;
                    }
                }
            }
        }
    }
}
