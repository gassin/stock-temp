<?php
namespace StockDomain\ValueObjects\StockType;

use StockDomain\Repository\SearchStockRepository\SearchStockRepository;
use StockDomain\SearchCondition\AbstractSearchCondition;
use StockDomain\ValueObjects\StockType\StockTypeVO;

class StockTypeWhereCondition extends AbstractSearchCondition
{
    private $conditions = [];

    public function __construct(array $conditions)
    {
        $this->conditions = $conditions;
        $this->generateStockTypeConditions();
    }

    private function generateStockTypeConditions()
    {
        $stockTypes = [];
        if (!empty($this->conditions[StockTypeVO::NAME])) {
            /**
             * @var int $key
             * @var StockTypeVO $condition
             */
            foreach ($this->conditions[StockTypeVO::NAME] as $key => $condition) {
                $paramName = ':'.StockTypeVO::NAME.'param'.$key;
                $stockTypes[$paramName] = SearchStockRepository::TABLE_ALIAS.'.'.StockTypeVO::NAME.' = '.$paramName;
                $this->whereParams[$paramName] = $condition->__toString();
            }
            $this->where = implode(' OR ', $stockTypes);
        }
    }
}
