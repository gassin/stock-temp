<?php
namespace StockDomain\ValueObjects\Quality;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Exception;
use StockDomain\SearchCondition\AbstractSearchCondition;

class QualityWhereConditionsMiddleware implements MiddlewareInterface
{
    private $requestParams = [];
    private $searchConditions = [];

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $this->requestParams = $request->getParsedBody();
        $this->processQuality();

        $whereConditions = $request->getAttribute(AbstractSearchCondition::WHERE_CONDITIONS) ?? [];

        if ($this->searchConditions) {
            $whereConditions[QualityVO::NAME] = new QualityWhereCondition($this->searchConditions);
        }

        return $handler->handle($request->withAttribute(AbstractSearchCondition::WHERE_CONDITIONS, $whereConditions));
    }


    private function processQuality()
    {
        if (!empty($this->requestParams[QualityVO::NAME])) {
            if (is_array($this->requestParams[QualityVO::NAME])) {
                foreach ($this->requestParams[QualityVO::NAME] as $sku) {
                    try {
                        $this->searchConditions[QualityVO::NAME][] = QualityVO::create($sku);
                    } catch (Exception $exception) {
                        throw $exception;
                    }
                }
            }
        }
    }
}
