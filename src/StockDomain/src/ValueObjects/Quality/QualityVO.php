<?php
namespace StockDomain\ValueObjects\Quality;

use StockDomain\Interfaces\IGet;
use StockDomain\ValueObjects\Quality\QualityNotFoundException;
use JsonSerializable;

class QualityVO implements JsonSerializable, IGet
{
    const NAME = 'quality';

    /** @var string $quality */
    private $quality;

    const QUALITY_NORMAL = 1;
    const QUALITY_DEFECT = 2;
    const QUALITY_QUARANTINE = 3;
    const QUALITY_OVERAGE = 4;
    const QUALITY_DAMAGED_OVERAGE = 5;
    const QUALITY_NOT_FOUND = 6;
    const QUALITY_PROD_DEFECT = 7;
    const QUALITY_INCOMPLETE_SET = 31;
    const QUALITY_REPAIR = 32;
    const QUALITY_TRASH = 21;

    const QUALITY_ALL = 1000;

    private static $mapQuality = [
        self::QUALITY_NORMAL => 'normal',
        self::QUALITY_DEFECT => 'defect',
        self::QUALITY_QUARANTINE => 'quarantine',
        self::QUALITY_OVERAGE => 'overage',
        self::QUALITY_DAMAGED_OVERAGE => 'damaged overage',
        self::QUALITY_NOT_FOUND => 'not found',
        self::QUALITY_PROD_DEFECT => 'pro defect',
        self::QUALITY_INCOMPLETE_SET => 'incomplete set',
        self::QUALITY_REPAIR => 'repair',
        self::QUALITY_TRASH => 'trash'
    ];

    private static $mapQualityShortTitle = [
        self::QUALITY_ALL => 'a',
        self::QUALITY_NORMAL => 'n',
        self::QUALITY_DEFECT => 'd',
        self::QUALITY_QUARANTINE => 'q',
        self::QUALITY_OVERAGE => 'o',
        self::QUALITY_DAMAGED_OVERAGE => 'do',
        self::QUALITY_NOT_FOUND => 'nf',
        self::QUALITY_PROD_DEFECT => 'pd',
        self::QUALITY_INCOMPLETE_SET => 'is',
        self::QUALITY_REPAIR => 'r',
        self::QUALITY_TRASH => 't'
    ];

    /**
     * QualityVO constructor.
     * @param int $quality
     * @throws \StockDomain\ValueObjects\Quality\QualityNotFoundException
     */
    public function __construct(int $quality)
    {
        try {
            $this->validate($quality);
        } catch (QualityNotFoundException $qualityNotFoundException) {
            throw $qualityNotFoundException;
        }

        $this->quality = (int) $quality;
    }

    /**
     * @return int
     */
    public function get(): int
    {
        return $this->quality;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->quality;
    }

    /**
     * @return int|mixed
     */
    public function jsonSerialize()
    {
        return $this->get();
    }

    /**
     * @param int $quality
     * @return bool
     * @throws \StockDomain\ValueObjects\Quality\QualityNotFoundException
     */
    private function validate(int $quality) : bool
    {
        if (!array_key_exists($quality, self::$mapQuality)) {
            throw new QualityNotFoundException();
        }

        return true;
    }

    /**
     * @return array
     */
    public static function getAvailableQualities() : array
    {
        return array_keys(self::$mapQuality);
    }

    /**
     * @param int $quality
     * @return QualityVO
     * @throws \StockDomain\ValueObjects\Quality\QualityNotFoundException
     */
    public static function create(int $quality) : QualityVO
    {
        try {
            return new self($quality);
        } catch (QualityNotFoundException $qualityNotFoundException) {
            throw $qualityNotFoundException;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param QualityVO $qualityVO
     * @return bool
     */
    public function equals(QualityVO $qualityVO) : bool
    {
        return $this->get() === $qualityVO->get();
    }
}
