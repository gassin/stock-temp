<?php
namespace StockDomain\ValueObjects\Quality;

use StockDomain\Repository\SearchStockRepository\SearchStockRepository;
use StockDomain\SearchCondition\AbstractSearchCondition;

class QualityWhereCondition extends AbstractSearchCondition
{

    private $conditions = [];

    public function __construct(array $conditions)
    {
        $this->conditions = $conditions;
        $this->generateQualityConditions();
    }

    private function generateQualityConditions()
    {
        $skus = [];
        if (!empty($this->conditions[QualityVO::NAME])) {
            /**
             * @var int $key
             * @var QualityVO $condition
             */
            foreach ($this->conditions[QualityVO::NAME] as $key => $condition) {
                $paramName = ':'.QualityVO::NAME.'param'.$key;
                $skus[$paramName] = SearchStockRepository::TABLE_ALIAS.'.'.QualityVO::NAME.' = '.$paramName;
                $this->whereParams[$paramName] = $condition->get();
            }
            $this->where = implode(' or ', $skus);
        }
    }
}
