<?php
namespace StockDomain\ValueObjects\Quality;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use StockDomain\ValueObjects\Quality\QualityVO;

/**
 * Quantity data type
 */
class QualityType extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getIntegerTypeDeclarationSQL($fieldDeclaration);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        try {
            return new QualityVO($value);
        } catch (\Exception $exception) {
        }
    }

    /**
     * @param QualityVO $value
     * @param AbstractPlatform $platform
     * @return mixed
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value->get();
    }

    public function getName()
    {
        return QualityVO::NAME;
    }
}
