<?php
namespace StockDomain\Service\ValidateSourceStockType;

use StockDomain\ValueObjects\Source\SourceVO;
use StockDomain\ValueObjects\StockType\StockTypeVO;

class ValidateSourceStockType
{
    public function __invoke(SourceVO $source, StockTypeVO $stockType) : bool
    {
        return (
        (
            $source->isClub() &&
            ($stockType->isPhysical() || $stockType->isVirtualClub())
        ) ||
        (
            $source->isShop() &&
            ($stockType->isVirtualShop() || $stockType->isPhysical())
        ) ||
        $source->isDelta()
        );
    }
}
