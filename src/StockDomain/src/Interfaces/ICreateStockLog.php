<?php
namespace StockDomain\Interfaces;

use StockDomain\Entity\StockLog;
use StockDomain\ValueObjects\Location\LocationVO;
use StockDomain\ValueObjects\Quality\QualityVO;
use StockDomain\ValueObjects\Quantity\QuantityVO;
use StockDomain\ValueObjects\Sku\SkuVO;
use StockDomain\ValueObjects\StockType\StockTypeVO;
use StockDomain\ValueObjects\User\UserVO;

interface ICreateStockLog
{
    /**
     * @param SkuVO $sku
     * @param QuantityVO $quantity
     * @param QualityVO $quality
     * @param StockTypeVO $stockType
     * @param LocationVO $location
     * @param UserVO $user
     * @return StockLog
     */
    public function create(
        SkuVO $sku,
        QuantityVO $quantity,
        QualityVO $quality,
        StockTypeVO $stockType,
        LocationVO $location,
        UserVO $user
    ) : StockLog;
}
