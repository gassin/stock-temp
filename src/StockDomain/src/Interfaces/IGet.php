<?php
namespace StockDomain\Interfaces;

interface IGet
{
    public function get();
}
