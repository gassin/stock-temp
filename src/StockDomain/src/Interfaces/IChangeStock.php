<?php
namespace StockDomain\Interfaces;

use StockDomain\ValueObjects\Location\LocationVO;
use StockDomain\ValueObjects\Quality\QualityVO;
use StockDomain\ValueObjects\Quantity\QuantityVO;
use StockDomain\ValueObjects\Sku\SkuVO;
use StockDomain\ValueObjects\StockType\StockTypeVO;

interface IChangeStock
{
    public function changeStock(
        SkuVO $sku,
        QuantityVO $quantity,
        QualityVO $quality,
        StockTypeVO $stockType,
        LocationVO $location
    ) : void;
}
