<?php

declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Zend\Expressive\Application;
use Zend\Expressive\MiddlewareFactory;

use Zend\Expressive\Helper\BodyParams\BodyParamsMiddleware;
use StockDomain\Entity\Manager\FlushMiddleware\FlushMiddleware;
use StockDomain\Entity\Middleware\StockLog\StockLogCreateEntityMiddleware;

use Zend\ProblemDetails\ProblemDetailsMiddleware;
use StockAPI\V1\Stock\PutRequestValidateParamsMiddleware\PutRequestValidateParamsMiddleware;

use StockDomain\ValueObjects\Sku\SkuWhereConditionsMiddleware;
use StockDomain\ValueObjects\StockType\StockTypeWhereConditionsMiddleware;
use StockDomain\ValueObjects\Quality\QualityWhereConditionsMiddleware;
use StockDomain\ValueObjects\Location\LocationWhereConditionsMiddleware;
use StockAPI\V1\Search\Handler\SearchStockHandler;
use StockAPI\V1\Response\NoDataMiddleware;
use StockAPI\V1\Stock\CreateValueObjectsInRequestMiddleware;
use StockAPI\V1\Stock\Quantity\CalculateDeltaMiddleware;
use StockAPI\V1\Club\StopUpdate\StopUpdateMiddleware;
use StockAPI\V1\Auth\Token\ValidateTokenMiddleware;
use StockAPI\V1\User\CreateUserMiddleware;
use StockDomain\ValueObjects\Sku\SkuMiddleware;
use StockAPI\V1\Club\SetQuantityZero\CreateValueObjectsForQuantityZeroMiddleware;
use StockAPI\V1\Club\ClearStopSkuFromRequest\ClearStopSkuFromRequestMiddleware;
use StockAPI\V1\Validation\SourceStockType\SourceStockTypeValidate;
use StockAPI\V1\Club\StopUpdate\ValidateSource\ValidateSource;
use StockAPI\V1\Validation\NegativeQuantity\NegativeQuantityValidateParamsMiddleware;

/**
 * Setup routes with a single request method:
 *
 * $app->get('/', App\Handler\HomePageHandler::class, 'home');
 * $app->post('/album', App\Handler\AlbumCreateHandler::class, 'album.create');
 * $app->put('/album/:id', App\Handler\AlbumUpdateHandler::class, 'album.put');
 * $app->patch('/album/:id', App\Handler\AlbumUpdateHandler::class, 'album.patch');
 * $app->delete('/album/:id', App\Handler\AlbumDeleteHandler::class, 'album.delete');
 *
 * Or with multiple request methods:
 *
 * $app->route('/contact', App\Handler\ContactHandler::class, ['GET', 'POST', ...], 'contact');
 *
 * Or handling all request methods:
 *
 * $app->route('/contact', App\Handler\ContactHandler::class)->setName('contact');
 *
 * or:
 *
 * $app->route(
 *     '/contact',
 *     App\Handler\ContactHandler::class,
 *     Zend\Expressive\Router\Route::HTTP_METHOD_ANY,
 *     'contact'
 * );
 */
return function (Application $app, MiddlewareFactory $factory, ContainerInterface $container) : void {
    $app->put(
        '/api/v1/stock',
        [
            ProblemDetailsMiddleware::class,
            BodyParamsMiddleware::class,
            ValidateTokenMiddleware::class,
            CreateUserMiddleware::class,
            PutRequestValidateParamsMiddleware::class,
            CreateValueObjectsInRequestMiddleware::class,
            SourceStockTypeValidate::class,
            ClearStopSkuFromRequestMiddleware::class,
            StockLogCreateEntityMiddleware::class,
            FlushMiddleware::class,
            NoDataMiddleware::class
        ],
        'api.v1.stock.put'
    );
    $app->put(
        '/api/v1/stock/quantity',
        [
            ProblemDetailsMiddleware::class,
            BodyParamsMiddleware::class,
            ValidateTokenMiddleware::class,
            CreateUserMiddleware::class,
            PutRequestValidateParamsMiddleware::class,
            NegativeQuantityValidateParamsMiddleware::class,
            CreateValueObjectsInRequestMiddleware::class,
            SourceStockTypeValidate::class,
            ClearStopSkuFromRequestMiddleware::class,
            CalculateDeltaMiddleware::class,
            StockLogCreateEntityMiddleware::class,
            FlushMiddleware::class,
            NoDataMiddleware::class
        ],
        'api.v1.stock.set.quantity'
    );
    $app->put(
        '/api/v1/club/stock/stop',
        [
            ProblemDetailsMiddleware::class,
            BodyParamsMiddleware::class,
            ValidateTokenMiddleware::class,
            ValidateSource::class,
            CreateUserMiddleware::class,
            SkuMiddleware::class,
            StopUpdateMiddleware::class,
            CreateValueObjectsForQuantityZeroMiddleware::class,
            CalculateDeltaMiddleware::class,
            StockLogCreateEntityMiddleware::class,
            FlushMiddleware::class,
            NoDataMiddleware::class
        ],
        'api.v1.club.stock.stop'
    );
    $app->post('/api/v1/stock/search', [
        ProblemDetailsMiddleware::class,
        BodyParamsMiddleware::class,
        SkuWhereConditionsMiddleware::class,
        QualityWhereConditionsMiddleware::class,
        LocationWhereConditionsMiddleware::class,
        StockTypeWhereConditionsMiddleware::class,
        SearchStockHandler::class
    ]);
};
