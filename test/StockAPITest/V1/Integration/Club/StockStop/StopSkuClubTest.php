<?php
declare(strict_types = 1);

namespace StockAPITest\V1\Integration\Club\StockStop;

use Fig\Http\Message\StatusCodeInterface;
use StockDomain\ValueObjects\Sku\SkuVO;

class StopSkuClubTest extends AbstractStopStock
{
    public function setUp()
    {
        $this->clientConfig['auth-token'] = $this->getClubToken();
        parent::setUp();
    }

    public function testItSendsStopRequest()
    {
        $response = $this->sendClubStopSkuRequest([SkuVO::NAME => $this->randomSku]);
        $this->assertEquals(StatusCodeInterface::STATUS_NO_CONTENT, $response->getStatusCode());

        $this->processSearchResponseAfterStopSku(
            $this->getSearchResponseByRandomSku()
        );
    }

    public function testPutStockAndThenStopSku()
    {
        $this->sendClubStopSkuRequest([SkuVO::NAME => $this->randomSku]);
        $stockLog = $this->getStockLog($this->randomSku);
        $this->sendPutStockRequest(
            $stockLog->jsonSerialize()
        );

        $this->processSearchResponseAfterStopSku(
            $this->getSearchResponseByRandomSku()
        );
    }

    public function testPutStockQuantityAndThenStopSku()
    {
        $this->sendClubStopSkuRequest([SkuVO::NAME => $this->randomSku]);
        $stockLog = $this->getStockLog($this->randomSku);
        $this->sendPutQuantityStockRequest(
            $stockLog->jsonSerialize()
        );
        $this->processSearchResponseAfterStopSku(
            $this->getSearchResponseByRandomSku()
        );
    }
}