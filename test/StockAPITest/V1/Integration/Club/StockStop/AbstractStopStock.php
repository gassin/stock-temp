<?php
declare(strict_types = 1);

namespace StockAPITest\V1\Integration\Club\StockStop;

use StockAPITest\V1\Integration\AbstractStock;
use StockAPITest\V1\Integration\InitTokens;
use StockDomain\Entity\StockLog;
use StockDomain\Repository\StockLogRepository\StockLogCreateRepository;
use StockDomain\ValueObjects\Location\LocationVO;
use StockDomain\ValueObjects\Quality\QualityVO;
use StockDomain\ValueObjects\Quantity\QuantityVO;
use StockDomain\ValueObjects\Sku\SkuVO;
use StockDomain\ValueObjects\StockType\StockTypeVO;
use StockDomainTest\ValueObjects\ValueObjectDataProvider;

abstract class AbstractStopStock extends AbstractStock
{
    const TEST_SKU = 'STOP_SKU_';

    protected $randomSku;

    public function setUp()
    {
        $this->randomSku = self::TEST_SKU.mt_rand(100000, 1000000);
        parent::setUp();
    }

    protected function processSearchResponseAfterStopSku($searchResponse)
    {
        foreach ($this->processResult($searchResponse) as $item) {
            $this->assertEquals(0, $item->quantity);
        }
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function getSearchResponseByRandomSku()
    {
        return $this->sendSearchMessageRequest(
            [
                SkuVO::NAME  => [$this->randomSku]
            ]
        );
    }

    /**
     * @return StockLog
     * @throws \StockDomain\ValueObjects\Location\LocationNotFoundException
     * @throws \StockDomain\ValueObjects\Quality\QualityNotFoundException
     */
    protected function getStockLog() : StockLog
    {
        return StockLogCreateRepository::createByPrimitives(
            $this->randomSku,
            QuantityVO::create(10)->get(),
            QualityVO::create(QualityVO::QUALITY_NORMAL)->get(),
            StockTypeVO::create(StockTypeVO::VIRTUAL_CLUB)->get(),
            LocationVO::create(null)->get(),
            ValueObjectDataProvider::getTestUser()->__toString()
        );
    }
}