<?php
declare(strict_types = 1);

namespace StockAPITest\V1\Integration\Club\StockStop;

use GuzzleHttp\Exception\ClientException;
use StockDomain\ValueObjects\Sku\SkuVO;

class StopSkuDeltaTest extends AbstractStopStock
{
    public function setUp()
    {
        $this->clientConfig['auth-token'] = $this->getDeltaToken();
        parent::setUp();
    }

    public function testDeltaCanNotSendStopSkuRequest()
    {
        $this->expectException(ClientException::class);
        $this->sendClubStopSkuRequest([SkuVO::NAME => $this->randomSku]);
    }
}