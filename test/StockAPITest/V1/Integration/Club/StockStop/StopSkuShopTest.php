<?php
declare(strict_types = 1);

namespace StockAPITest\V1\Integration\Club\StockStop;

use Fig\Http\Message\StatusCodeInterface;
use GuzzleHttp\Exception\ClientException;
use StockDomain\ValueObjects\Sku\SkuVO;

class StopSkuShopTest extends AbstractStopStock
{
    public function setUp()
    {
        $this->clientConfig['auth-token'] = $this->getShopToken();
        parent::setUp();
    }

    public function testDeltaCanNotSendStopSkuRequest()
    {
        $this->expectException(ClientException::class);
        $this->sendClubStopSkuRequest([SkuVO::NAME => $this->randomSku]);
    }
}