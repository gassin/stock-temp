<?php
declare(strict_types = 1);

namespace StockAPITest\V1\Integration\SearchStock\StockPutSearch;

class StockPutSearchClubTest extends AbstractStockPutSearch
{
    public function setUp()
    {
        $this->clientConfig['auth-token'] = $this->getClubToken();
        parent::setUp();
    }

    /**
     * @param int $quantity
     * @param int $quality
     * @param string $stockType
     * @param null|string $location
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @dataProvider \StockDomainTest\ValueObjects\ValueObjectDataProvider::positiveCasesClub()
     */
    public function testItPutsToStockAndCompare(int $quantity, int $quality, string $stockType, ?string $location)
    {
        $initialSearchObject = $this->putStockSearchStockMakeAssertionsAndGetSearchResult(
            $quantity,
            $quality,
            $stockType,
            $location
        );
        $secondSearchObject = $this->putStockSearchStockMakeAssertionsAndGetSearchResult(
            $quantity,
            $quality,
            $stockType,
            $location
        );
        $this->assertEquals($secondSearchObject->quantity, $initialSearchObject->quantity + $quantity);
    }
}
