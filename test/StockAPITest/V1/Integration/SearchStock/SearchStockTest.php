<?php
namespace StockAPITest\V1\Integration\SearchStock;

use Fig\Http\Message\StatusCodeInterface;
use StockAPITest\V1\Integration\AbstractStock;
use StockDomain\ValueObjects\Location\LocationVO;
use StockDomain\ValueObjects\Quality\QualityVO;
use StockDomain\ValueObjects\StockType\StockTypeVO;

class SearchStockTest extends AbstractStock
{
    public function testLocationsReturnsOnlySearchedValue()
    {
        foreach (LocationVO::getAvailableLocationCodes() as $availableLocationCode) {
            $result = $this->sendSearchMessageRequest([
                LocationVO::NAME => [$availableLocationCode]
            ]);

            $this->assertEquals(StatusCodeInterface::STATUS_OK, $result->getStatusCode());

            foreach ($this->processResult($result) as $item) {
                $this->assertEquals($availableLocationCode, $item->location);
            }
        }
    }

    public function testQualitiesReturnsOnlySearchedValue()
    {
        foreach (QualityVO::getAvailableQualities() as $availableQuality) {
            $result = $this->sendSearchMessageRequest(
                [
                    QualityVO::NAME => [$availableQuality]
                ]
            );

            $this->assertEquals(StatusCodeInterface::STATUS_OK, $result->getStatusCode());

            foreach ($this->processResult($result) as $item) {
                $this->assertEquals($availableQuality, $item->quality);
            }
        }
    }

    public function testStockTypeReturnsOnlySearchedValue()
    {
        foreach (StockTypeVO::getAvailableTypes() as $availableType) {
            $result = $this->sendSearchMessageRequest(
                [
                    StockTypeVO::NAME => [$availableType]
                ]
            );

            $this->assertEquals(StatusCodeInterface::STATUS_OK, $result->getStatusCode());

            foreach ($this->processResult($result) as $item) {
                $this->assertEquals($availableType, $item->stockType);
            }
        }
    }
}
