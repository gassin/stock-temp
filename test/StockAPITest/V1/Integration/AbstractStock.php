<?php
namespace StockAPITest\V1\Integration;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use StockAPI\ConfigProvider;

class AbstractStock extends TestCase
{
    use MiddlewareAwareTestCaseTrait;
    use InitTokens;

    /** @var string $endpoint */
    protected $sstEndpoint;

    /** @var Client $client */
    protected $client;

    /** @var array $clientConfig */
    protected $clientConfig;

    /** @var array $tokens */
    protected $tokens;

    public function setUp()
    {
        $this->initApplication(
            __DIR__ . '/../../../../config/container.php',
            __DIR__.'/../../../../config/pipeline.php',
            __DIR__.'/../../../../config/routes.php'
        );
        $this->tokens = $this->container->get('config')['tokens'];
        $this->sstEndpoint = $this->container->get('config')['endpoint'];

        $config = [
            'headers' => [
                'Content-Type' => 'application/json'
            ],
        ];

        if (isset($this->clientConfig['auth-token'])) {
            $config['headers']['auth-token'] = $this->clientConfig['auth-token'];
        }

        if (isset($this->clientConfig['user'])) {
            $config['headers']['user'] = $this->clientConfig['user'];
        }

        if (isset($this->clientConfig['headers'])) {
            $config['headers'] = array_merge($this->clientConfig['headers'], $config['headers']);
        }

        if (isset($this->clientConfig['endPoint'])) {
            $config['base_uri'] = $this->clientConfig['endPoint'];
        }

        $this->client = new Client(
            $config
        );
    }

    /**
     * @param array $message
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function sendSearchMessageRequest(array $message) : ResponseInterface
    {
        return $this->client->request(
            'POST',
            $this->sstEndpoint.ConfigProvider::POST_SEARCH_STOCK_METHOD_PATH,
            [
                'json' => $message,
            ]
        );
    }

    /**
     * @param array $json
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function sendPutStockRequest(array $json) : ResponseInterface
    {
        return $this->client->request(
            'PUT',
            $this->sstEndpoint.ConfigProvider::PUT_STOCK_METHOD_PATH,
            [
                'json' => [$json],
            ]
        );
    }

    /**
     * @param array $json
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function sendPutQuantityStockRequest(array $json) : ResponseInterface
    {
        return $this->client->request(
            'PUT',
            $this->sstEndpoint.ConfigProvider::PUT_STOCK_QUANTITY_METHOD_PATH,
            [
                'json' => [$json],
            ]
        );
    }

    /**
     * @param array $json
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function sendClubStopSkuRequest(array $json) : ResponseInterface
    {
        return $this->client->request(
            'PUT',
            $this->sstEndpoint.ConfigProvider::PUT_CLUB_STOP_SKU_UPDATE_METHOD_PATH,
            [
                'json' => $json,
            ]
        );
    }

    /**
     * @param Response $result
     * @return mixed
     */
    protected function processResult($result)
    {
        return json_decode($result->getBody()->getContents());
    }
}
