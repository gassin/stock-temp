<?php
declare(strict_types = 1);

namespace StockAPITest\V1\Integration\StockQuantity\PutStockQuantity;

use GuzzleHttp\Exception\GuzzleException;

class PutStockQuantityDeltaTest extends AbstractPutStockQuantity
{
    public function setUp()
    {
        $this->clientConfig['auth-token'] = $this->getDeltaToken();
        parent::setUp();
    }

    /**
     * @dataProvider \StockDomainTest\ValueObjects\ValueObjectDataProvider::positiveCasesDelta()
     * @param int $quantity
     * @param int $quality
     * @param string $stockType
     * @param string|null $location
     * @throws GuzzleException
     */
    public function testItSendsToStock(int $quantity, int $quality, string $stockType, ?string $location)
    {
        $this->setQuantityToStock($quantity, $quality, $stockType, $location);
    }
}
