<?php
declare(strict_types = 1);

namespace StockAPITest\V1\Integration\StockQuantity\PutStockQuantity;

use Fig\Http\Message\StatusCodeInterface;
use GuzzleHttp\Exception\ClientException;
use StockAPI\V1\Validation\NegativeQuantity\NegativeQuantityException;
use StockAPITest\V1\Integration\AbstractStock;
use StockAPITest\V1\Integration\InitTokens;
use StockAPITest\V1\Integration\MiddlewareAwareTestCaseTrait;
use StockDomain\Repository\StockLogRepository\StockLogCreateRepository;
use StockDomainTest\ValueObjects\ValueObjectDataProvider;

class AbstractPutStockQuantity extends AbstractStock
{
    const TEST_SKU = 'SUPER_TEST_SKU';

    public function setQuantityToStock(int $quantity, int $quality, string $stockType, ?string $location)
    {
        $stockLog = StockLogCreateRepository::createByPrimitives(
            self::TEST_SKU,
            $quantity,
            $quality,
            $stockType,
            $location,
            ValueObjectDataProvider::getTestUser()->__toString()
        );

        $this->expectExceptionIfNegativeQuantity($quantity);
        $response = $this->sendPutQuantityStockRequest($stockLog->jsonSerialize());
        $this->makeAssertionByQuantity($quantity, $response);
    }

    protected function makeAssertionByQuantity(int $quantity, $response)
    {
        if ($quantity < 0) {
            $this->assertEquals(StatusCodeInterface::STATUS_BAD_REQUEST, $response->getStatusCode());
            return;
        }
        $this->assertEquals(StatusCodeInterface::STATUS_NO_CONTENT, $response->getStatusCode());
    }

    /**
     * @param int $quantity
     */
    protected function expectExceptionIfNegativeQuantity(int $quantity)
    {
        if ($quantity < 0) {
            $this->expectException(ClientException::class);
        }
    }
}