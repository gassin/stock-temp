<?php
declare(strict_types = 1);

namespace StockAPITest\V1\Integration\StockQuantity\PutStockQuantitySearch;

use GuzzleHttp\Exception\GuzzleException;

class PutStockQuantitySearchDeltaTest extends AbstractPutStockQuantitySearch
{
    public function setUp()
    {
        $this->clientConfig['auth-token'] = $this->getDeltaToken();
        parent::setUp();
    }

    /**
     * @param int $quantity
     * @param int $quality
     * @param string $stockType
     * @param null|string $location
     * @throws GuzzleException
     * @dataProvider \StockDomainTest\ValueObjects\ValueObjectDataProvider::positiveCasesDelta()
     */
    public function testItPutsToStockAndCompare(int $quantity, int $quality, string $stockType, ?string $location)
    {
        $result = $this->putStockSearchStockMakeAssertionsAndGetSearchResult(
            $quantity,
            $quality,
            $stockType,
            $location
        );
        $this->assertEquals($quantity, $result->quantity);
    }
}
