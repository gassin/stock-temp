<?php
declare(strict_types = 1);

namespace StockAPITest\V1\Integration\StockQuantity\PutStockQuantitySearch;

use Fig\Http\Message\StatusCodeInterface;
use GuzzleHttp\Exception\ClientException;
use StockAPITest\V1\Integration\AbstractStock;
use StockAPITest\V1\Integration\InitTokens;
use StockAPITest\V1\Integration\MiddlewareAwareTestCaseTrait;
use StockDomain\Repository\StockLogRepository\StockLogCreateRepository;
use StockDomain\ValueObjects\Location\LocationVO;
use StockDomain\ValueObjects\Quality\QualityVO;
use StockDomain\ValueObjects\Quantity\QuantityVO;
use StockDomain\ValueObjects\Sku\SkuVO;
use StockDomain\ValueObjects\StockType\StockTypeVO;
use StockDomainTest\ValueObjects\ValueObjectDataProvider;

class AbstractPutStockQuantitySearch extends AbstractStock
{
    const TEST_SKU = 'SUPER_TEST_SKU';

    /**
     * Method PUT to stock input params, then makes search request to stock with that params and make assertions
     * @param int $quantity
     * @param int $quality
     * @param string $stockType
     * @param null|string $location
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function putStockSearchStockMakeAssertionsAndGetSearchResult(
        int $quantity,
        int $quality,
        string $stockType,
        ?string $location
    ) {
        $this->putStock($quantity, $quality, $stockType, $location);
        $initialSearchResult = $this->processResult(
            $this->sendSearchMessageRequest([
                SkuVO::NAME => [self::TEST_SKU],
                QuantityVO::NAME => [$quantity],
                QualityVO::NAME => [$quality],
                StockTypeVO::NAME => [$stockType],
                LocationVO::NAME => [$location]
            ])
        );
        $this->assertCount(1, $initialSearchResult);
        $searchObject = array_shift($initialSearchResult);
        $this->assertEquals(self::TEST_SKU, $searchObject->sku);
        $this->assertEquals($quality, $searchObject->quality);
        $this->assertEquals($stockType, $searchObject->stockType);
        $this->assertEquals($location, $searchObject->location);
        return $searchObject;
    }

    /**
     * @param $quantity
     * @param $quality
     * @param $stockType
     * @param null $location
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function putStock(int $quantity, int $quality, string $stockType, ?string $location)
    {
        $stockLog = StockLogCreateRepository::createByPrimitives(
            self::TEST_SKU,
            $quantity,
            $quality,
            $stockType,
            $location,
            ValueObjectDataProvider::getTestUser()->__toString()
        );
        $this->expectExceptionIfNegativeQuantity($quantity);

        return $this->sendPutQuantityStockRequest($stockLog->jsonSerialize());
    }

    /**
     * @param int $quantity
     */
    protected function expectExceptionIfNegativeQuantity(int $quantity)
    {
        if ($quantity < 0) {
            $this->expectException(ClientException::class);
        }
    }
}