<?php
declare(strict_types = 1);

namespace StockAPITest\V1\Integration\StockQuantity\PutStockQuantitySearch;

use GuzzleHttp\Exception\GuzzleException;

class PutStockQuantitySearchShopTest extends AbstractPutStockQuantitySearch
{
    public function setUp()
    {
        $this->clientConfig['auth-token'] = $this->getShopToken();
        parent::setUp();
    }

    /**
     * @param int $quantity
     * @param int $quality
     * @param string $stockType
     * @param null|string $location
     * @throws GuzzleException
     * @dataProvider \StockDomainTest\ValueObjects\ValueObjectDataProvider::positiveCasesShop()
     */
    public function testItPutsToStockAndCompare(int $quantity, int $quality, string $stockType, ?string $location)
    {
        $result = $this->putStockSearchStockMakeAssertionsAndGetSearchResult(
            $quantity,
            $quality,
            $stockType,
            $location
        );
        $this->assertEquals($quantity, $result->quantity);
    }
}
