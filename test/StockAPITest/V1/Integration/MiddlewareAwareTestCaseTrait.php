<?php
namespace StockAPITest\V1\Integration;

use Psr\Container\ContainerInterface;
use Zend\Expressive\Application;
use Zend\Expressive\MiddlewareFactory;

trait MiddlewareAwareTestCaseTrait
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var Application
     */
    private $application;

    protected function initApplication(
        string $containerConfigPath,
        string $pipelineConfigPath = null,
        string $routesConfigPath = null
    ) {
        $this->container = require $containerConfigPath;
        $this->application = $this->container->get(Application::class);
        $factory = $this->container->get(MiddlewareFactory::class);

        if (null !== $pipelineConfigPath) {
            (require $pipelineConfigPath)($this->application, $factory, $this->container);
        }
        if (null !== $routesConfigPath) {
            (require $routesConfigPath)($this->application, $factory, $this->container);
        }
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    /**
     * @return Application
     */
    public function getApplication(): Application
    {
        return $this->application;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->getContainer()->get($id);
    }
}