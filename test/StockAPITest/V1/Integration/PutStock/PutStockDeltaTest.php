<?php
declare(strict_types = 1);

namespace StockAPITest\V1\Integration\PutStock;

use GuzzleHttp\Exception\GuzzleException;

class PutStockDeltaTest extends AbstractPutStock
{
    public function setUp()
    {
        $this->clientConfig['auth-token'] = $this->getDeltaToken();
        parent::setUp();
    }

    /**
     * @param int $quantity
     * @param int $quality
     * @param string $stockType
     * @param string|null $location
     * @throws GuzzleException
     * @dataProvider \StockDomainTest\ValueObjects\ValueObjectDataProvider::positiveCasesDelta()
     */
    public function testItSendsToStock(int $quantity, int $quality, string $stockType, ?string $location)
    {
        $this->putToStock($quantity, $quality, $stockType, $location);
    }
}
