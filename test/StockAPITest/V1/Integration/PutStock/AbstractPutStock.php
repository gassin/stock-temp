<?php
declare(strict_types = 1);

namespace StockAPITest\V1\Integration\PutStock;

use Fig\Http\Message\StatusCodeInterface;
use StockAPITest\V1\Integration\AbstractStock;
use StockAPITest\V1\Integration\InitTokens;
use StockAPITest\V1\Integration\MiddlewareAwareTestCaseTrait;
use StockDomain\Repository\StockLogRepository\StockLogCreateRepository;
use StockDomainTest\ValueObjects\ValueObjectDataProvider;

abstract class AbstractPutStock extends AbstractStock
{
    const TEST_SKU = 'TEST_SKU';

    public function putToStock(int $quantity, int $quality, string $stockType, ?string $location)
    {
        $stockLog = StockLogCreateRepository::createByPrimitives(
            self::TEST_SKU,
            $quantity,
            $quality,
            $stockType,
            $location,
            ValueObjectDataProvider::getTestUser()->__toString()
        );

        $response = $this->sendPutStockRequest($stockLog->jsonSerialize());

        $this->assertEquals(StatusCodeInterface::STATUS_NO_CONTENT, $response->getStatusCode());
    }
}