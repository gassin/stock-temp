<?php
declare(strict_types = 1);

namespace StockAPITest\V1\Integration\PutStock;

use GuzzleHttp\Exception\GuzzleException;

class PutStockShopTest extends AbstractPutStock
{
    public function setUp()
    {
        $this->clientConfig['auth-token'] = $this->getShopToken();
        parent::setUp();
    }

    /**
     * @param int $quantity
     * @param int $quality
     * @param string $stockType
     * @param string|null $location
     * @throws GuzzleException
     * @dataProvider \StockDomainTest\ValueObjects\ValueObjectDataProvider::positiveCasesShop()
     */
    public function testItSendsToStock(int $quantity, int $quality, string $stockType, ?string $location)
    {
        $this->putToStock($quantity, $quality, $stockType, $location);
    }
}
