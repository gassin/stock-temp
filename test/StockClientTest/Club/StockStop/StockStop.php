<?php
namespace StockClientTest\Club\StockStop;

use StockClient\StockLog\StockLogCollection;
use StockClientTest\AbstractClientTest;
use StockClientTest\PositiveCases;

abstract class StockStop extends AbstractClientTest
{
    const SKU = 'sku';

    protected $randomSku;

    public function setUp()
    {
        $this->randomSku = self::SKU.mt_rand(100000, 1000000);
        parent::setUp();
    }

    protected function processSearchResponseAfterStopSku($searchResponse)
    {
        foreach ($this->processResult($searchResponse) as $item) {
            $this->assertEquals(0, $item->quantity);
        }
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function getSearchResponseByRandomSku()
    {
        return $this->sendSearchMessage(
            [
                self::SKU  => [$this->randomSku]
            ]
        );
    }

    /**
     * @return StockLogCollection
     */
    protected function getStockLogCollection()
    {
        return StockLogCollection::create([
            [
                'sku' => $this->randomSku,
                'quantity' => 10,
                'quality' => 1, // normal
                'stockType' => PositiveCases::VIRTUAL_CLUB,
                'location' => null
            ]
        ]);
    }
}