<?php
namespace StockClientTest\Club\StockStop;

use GuzzleHttp\Exception\ClientException;

class StockStopClientShopTest extends StockStop
{
    public function setUp()
    {
        parent::setUp();
        $this->clientConfig[self::AUTH_TOKEN] = $this->getShopToken();
    }

    public function testDeltaCanNotSendStopSkuRequest()
    {
        $this->expectException(ClientException::class);
        $this->sendClubStopSkuRequest($this->randomSku);
    }
}
