<?php
namespace StockClientTest\Club\StockStop;

use Fig\Http\Message\StatusCodeInterface;
use StockClient\StockLog\StockLogClient;
use StockClient\StockLog\StockLogQuantityClient;

class StockLogClientClubTest extends StockStop
{
    public function setUp()
    {
        parent::setUp();
        $this->clientConfig[self::AUTH_TOKEN] = $this->getClubToken();
    }

    public function testItSendsStopRequest()
    {
        $response = $this->sendClubStopSkuRequest($this->randomSku);
        $this->assertEquals(StatusCodeInterface::STATUS_NO_CONTENT, $response->getStatusCode());

        $this->processSearchResponseAfterStopSku(
            $this->getSearchResponseByRandomSku()
        );
    }

    public function testPutStockAndThenStopSku()
    {
        $this->sendClubStopSkuRequest($this->randomSku);
        StockLogClient::put(
            $this->getStockLogCollection(),
            $this->clientConfig
        );

        $this->processSearchResponseAfterStopSku(
            $this->getSearchResponseByRandomSku()
        );
    }

    public function testPutStockQuantityAndThenStopSku()
    {
        $this->sendClubStopSkuRequest($this->randomSku);
        StockLogQuantityClient::put(
            $this->getStockLogCollection(),
            $this->clientConfig
        );
        $this->processSearchResponseAfterStopSku(
            $this->getSearchResponseByRandomSku()
        );
    }
}