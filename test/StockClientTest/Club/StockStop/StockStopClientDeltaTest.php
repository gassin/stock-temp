<?php
namespace StockClientTest\Club\StockStop;

use GuzzleHttp\Exception\ClientException;

class StockStopClientDeltaTest extends StockStop
{
    public function setUp()
    {
        parent::setUp();
        $this->clientConfig[self::AUTH_TOKEN] = $this->getDeltaToken();
    }

    public function testDeltaCanNotSendStopSkuRequest()
    {
        $this->expectException(ClientException::class);
        $this->sendClubStopSkuRequest($this->randomSku);
    }
}
