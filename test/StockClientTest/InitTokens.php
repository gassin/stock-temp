<?php
declare(strict_types = 1);

namespace StockClientTest;

trait InitTokens
{
    /**
     * @return mixed
     */
    public function getDeltaToken() : string
    {
        return $this->getTokens()['delta'];
    }

    /**
     * @return mixed
     */
    public function getShopToken() : string
    {
        return $this->getTokens()['shop'];
    }

    /**
     * @return mixed
     */
    public function getClubToken() : string
    {
        return $this->getTokens()['club'];
    }

    /**
     * @return mixed
     */
    private function getTokens() : array
    {
        $testConfig = require __DIR__ . '/../../config/autoload/test.local.php';
        return $testConfig['tokens'];
    }
}