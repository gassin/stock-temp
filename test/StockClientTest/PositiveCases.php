<?php
namespace StockClientTest;

use Exception;

class PositiveCases
{
    const SOURCE_CLUB = 'club';
    const SOURCE_SHOP = 'shop';
    const SOURCE_DELTA = 'delta';
    const QUALITY_NAME = 'quality';
    const STOCK_TYPE_NAME = 'stockType';
    const LOCATION_NAME = 'location';

    private static $availableLocationCodes = [
        1015 => 'RLC',
        1016 => 'HUB',
        1012 => 'WHPSTUDIO'
    ];

    const VIRTUAL_CLUB = 'virtualClubStock';
    const VIRTUAL_SHOP = 'virtualShopStock';
    const PHYSICAL_FREE = 'physicalFreeStock';

    /** @var array $availableStockTypes */
    private static $availableStockTypes = [
        self::VIRTUAL_CLUB,
        self::VIRTUAL_SHOP,
        self::PHYSICAL_FREE
    ];

    private static $mapQuality = [
        1 => 'normal',
        2 => 'defect',
        3 => 'quarantine',
        4 => 'overage',
        5 => 'damaged overage',
        6 => 'not found',
        7 => 'pro defect',
        31 => 'incomplete set',
        32 => 'repair',
        21 => 'trash'
    ];

    /**
     * @return array
     */
    public static function getAvailableLocationCodes()
    {
        return array_keys(self::$availableLocationCodes);
    }

    /**
     * @return array
     */
    public static function getAvailableTypes()
    {
        return self::$availableStockTypes;
    }

    /**
     * @return array
     */
    public static function getAvailableQualities()
    {
        return array_keys(self::$mapQuality);
    }

    /**
     * @return array
     */
    /*public static function get()
    {
        $result = [];

        foreach (self::getAvailableLocationCodes() as $location) {
            foreach (self::getAvailableTypes() as $stockType) {
                foreach (self::getAvailableQualities() as $quality) {
                    foreach ([1,-1] as $quantity) {
                        $result[] = [$quantity, $quality, $stockType, $location];
                    }
                }
            }
        }

        return $result;
    }*/

    /**
     * @return array
     * @throws Exception
     */
    public static function positiveCasesClub()
    {
        $result = [];

        foreach (self::getAvailableLocationCodes() as $location) {
            foreach (self::getAvailableQualities() as $quality) {
                foreach ([1, -1] as $quantity) {
                    foreach (self::getAvailableTypesBySource(self::SOURCE_CLUB) as $stockType) {
                        $result[] = [$quantity, $quality, $stockType, $location];
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @return array
     * @throws Exception
     */
    public static function positiveCasesShop()
    {
        $result = [];

        foreach (self::getAvailableLocationCodes() as $location) {
            foreach (self::getAvailableQualities() as $quality) {
                foreach ([1, -1] as $quantity) {
                    foreach (self::getAvailableTypesBySource(self::SOURCE_SHOP) as $stockType) {
                        $result[] = [$quantity, $quality, $stockType, $location];
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @return array
     * @throws Exception
     */
    public static function positiveCasesDelta()
    {
        $result = [];

        foreach (self::getAvailableLocationCodes() as $location) {
            foreach (self::getAvailableQualities() as $quality) {
                foreach ([1, -1] as $quantity) {
                    foreach (self::getAvailableTypesBySource(self::SOURCE_DELTA) as $stockType) {
                        $result[] = [$quantity, $quality, $stockType, $location];
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @return array
     * @throws Exception
     */
    public static function getSourceStockTypesCombinations()
    {
        $result = [];
        foreach (self::getAvailableSources() as $availableSource) {
            $result[] = self::getAvailableTypesBySource($availableSource);
        }
        return $result;
    }

    /**
     * @return array
     */
    public static function getAvailableSources()
    {
        return [
            self::SOURCE_CLUB,
            self::SOURCE_SHOP,
            self::SOURCE_DELTA
        ];
    }

    /**
     * @param string $source
     * @return array
     * @throws Exception
     */
    public static function getAvailableTypesBySource($source)
    {
        if ($source === self::SOURCE_CLUB) {
            return [
                self::VIRTUAL_CLUB,
                self::PHYSICAL_FREE
            ];
        } elseif ($source === self::SOURCE_SHOP) {
            return [
                self::VIRTUAL_SHOP,
                self::PHYSICAL_FREE
            ];
        } elseif ($source === self::SOURCE_DELTA) {
            return [
                self::VIRTUAL_SHOP,
                self::VIRTUAL_CLUB,
                self::PHYSICAL_FREE,
            ];
        }
    }
}