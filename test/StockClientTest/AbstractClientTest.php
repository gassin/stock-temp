<?php
namespace StockClientTest;

use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use StockClient\StockLog\Club\SkuCollection;
use StockClient\StockLog\StockSearchClient;
use StockClient\StockLog\Club\StockStopSkuUpdateClient;

class AbstractClientTest extends TestCase
{
    use InitTokens;

    const SKU = 'SUPER_TEST_SKU';

    const AUTH_TOKEN = 'auth-token';

    /** @var ContainerInterface $container */
    protected $container;

    /** @var string $endPoint */
    protected $endPoint;

    protected $clientConfig;

    /**
     * Initialize required configs and classes
     */
    public function setUp()
    {
        $this->clientConfig = require __DIR__ . '/../../config/autoload/client.config.local.php';
        $this->endPoint = $this->clientConfig['endPoint'];
    }

    public function testEndpointExist()
    {
        $this->assertNotEmpty($this->endPoint);
    }

    /**
     * @param Response $result
     * @return mixed
     */
    protected function processResult($result)
    {
        return json_decode($result->getBody()->getContents());
    }

    /**
     * @param array $message
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function sendSearchMessage($message = [])
    {
        return StockSearchClient::post(
            $message,
            $this->clientConfig
        );
    }

    /**
     * @param string $sku
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function sendClubStopSkuRequest($sku)
    {
        return StockStopSkuUpdateClient::put(
            SkuCollection::create([$sku]),
            $this->clientConfig
        );
    }
}