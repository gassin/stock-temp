<?php
namespace StockClientTest\StockQuantity;

use Fig\Http\Message\StatusCodeInterface;
use StockClient\StockLog\StockLogClient;
use StockClient\StockLog\StockLogCollection;
use StockClient\StockLog\StockLogQuantityClient;
use StockClientTest\AbstractClientTest;
use StockClientTest\PositiveCases;
use StockClientTest\StockQuantity\PutStockQuantity\PutStockQuantity;

class StockLogQuantityClientDeltaTest extends AbstractClientTest
{
    use PutStockQuantity;

    public function setUp()
    {
        parent::setUp();
        $this->clientConfig[self::AUTH_TOKEN] = $this->getDeltaToken();
    }

    /**
     * @dataProvider \StockClientTest\PositiveCases::positiveCasesDelta()
     * @param int $quantity
     * @param int $quality
     * @param string $stockType
     * @param null|string $location
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testPutAllCombinationsWorks($quantity, $quality, $stockType, $location = null)
    {
        $this->setQuantityToStock($quantity, $quality, $stockType, $location);
    }
}