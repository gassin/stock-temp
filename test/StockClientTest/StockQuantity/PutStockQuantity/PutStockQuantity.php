<?php
namespace StockClientTest\StockQuantity\PutStockQuantity;

use Fig\Http\Message\StatusCodeInterface;
use GuzzleHttp\Exception\ClientException;
use StockClient\StockLog\StockLogCollection;
use StockClient\StockLog\StockLogQuantityClient;

trait PutStockQuantity
{
    public function setQuantityToStock(int $quantity, int $quality, string $stockType, ?string $location)
    {
        $stockLogCollection = StockLogCollection::create([
            [
                'sku' => self::SKU,
                'quantity' => $quantity,
                'quality' => $quality,
                'stockType' => $stockType,
                'location' => $location
            ]
        ]);

        $this->expectExceptionIfNegativeQuantity($quantity);
        $response = StockLogQuantityClient::put(
            $stockLogCollection,
            $this->clientConfig
        );
        $this->makeAssertionByQuantity($quantity, $response);
    }

    protected function makeAssertionByQuantity($quantity, $response)
    {
        if ($quantity < 0) {
            $this->assertEquals(StatusCodeInterface::STATUS_BAD_REQUEST, $response->getStatusCode());
            return;
        }
        $this->assertEquals(StatusCodeInterface::STATUS_NO_CONTENT, $response->getStatusCode());
    }

    /**
     * @param int $quantity
     */
    protected function expectExceptionIfNegativeQuantity($quantity)
    {
        if ($quantity < 0) {
            $this->expectException(ClientException::class);
        }
    }
}