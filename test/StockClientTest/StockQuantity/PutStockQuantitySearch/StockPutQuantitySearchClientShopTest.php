<?php
namespace StockClientTest\StockQuantity;

use StockClient\StockLog\StockLogClient;
use StockClient\StockLog\StockLogCollection;
use StockClient\StockLog\StockLogQuantityClient;
use StockClientTest\AbstractClientTest;
use StockClientTest\StockQuantity\PutStockQuantitySearch\PutStockQuantitySearch;

class StockPutQuantitySearchClientShopTest extends AbstractClientTest
{
    use PutStockQuantitySearch;

    public function setUp()
    {
        parent::setUp();
        $this->clientConfig[self::AUTH_TOKEN] = $this->getShopToken();
    }

    /**
     * @param int $quantity
     * @param int $quality
     * @param string $stockType
     * @param null|string $location
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @dataProvider \StockClientTest\PositiveCases::positiveCasesShop()
     */
    public function testItPutsToStockAndCompare($quantity, $quality, $stockType, $location = null)
    {
        $secondSearchObject = $this->putStockSearchStockMakeAssertionsAndGetSearchResult(
            $quantity,
            $quality,
            $stockType,
            $location
        );
        $this->assertEquals($quantity, $secondSearchObject->quantity);
    }
}
