<?php
namespace StockClientTest\StockQuantity;

use StockClient\StockLog\StockLogClient;
use StockClient\StockLog\StockLogCollection;
use StockClient\StockLog\StockLogQuantityClient;
use StockClientTest\AbstractClientTest;
use StockClientTest\StockQuantity\PutStockQuantitySearch\PutStockQuantitySearch;

class StockPutQuantitySearchClientClubTest extends AbstractClientTest
{
    use PutStockQuantitySearch;

    public function setUp()
    {
        parent::setUp();
        $this->clientConfig[self::AUTH_TOKEN] = $this->getClubToken();
    }

    /**
     * @param int $quantity
     * @param int $quality
     * @param string $stockType
     * @param null|string $location
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @dataProvider \StockClientTest\PositiveCases::positiveCasesClub()
     */
    public function testItPutsToStockAndCompare($quantity, $quality, $stockType, $location = null)
    {
        $secondSearchObject = $this->putStockSearchStockMakeAssertionsAndGetSearchResult(
            $quantity,
            $quality,
            $stockType,
            $location
        );
        $this->assertEquals($quantity, $secondSearchObject->quantity);
    }
}
