<?php
namespace StockClientTest\StockQuantity\PutStockQuantitySearch;

use GuzzleHttp\Exception\ClientException;
use StockClient\StockLog\StockLogCollection;
use StockClient\StockLog\StockLogQuantityClient;

trait PutStockQuantitySearch
{
    /**
     * Method PUT to stock input params, then makes search request to stock with that params and make assertions
     * @param int $quantity
     * @param int $quality
     * @param string $stockType
     * @param null|string $location
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function putStockSearchStockMakeAssertionsAndGetSearchResult(
        $quantity,
        $quality,
        $stockType,
        $location = null
    ) {
        $this->putStock($quantity, $quality, $stockType, $location);
        $initialSearchResult = $this->processResult(
            $this->sendSearchMessage([
                'sku' => [self::SKU],
                'quantity' => [$quantity],
                'quality' => [$quality],
                'stockType' => [$stockType],
                'location' => [$location]
            ])
        );
        $this->assertCount(1, $initialSearchResult);
        $searchObject = array_shift($initialSearchResult);
        $this->assertEquals(self::SKU, $searchObject->sku);
        $this->assertEquals($quality, $searchObject->quality);
        $this->assertEquals($stockType, $searchObject->stockType);
        $this->assertEquals($location, $searchObject->location);
        return $searchObject;
    }

    /**
     * @param $quantity
     * @param $quality
     * @param $stockType
     * @param null $location
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function putStock($quantity, $quality, $stockType, $location = null)
    {
        $stockLogCollection = StockLogCollection::create([
            [
                'sku' => self::SKU,
                'quantity' => $quantity,
                'quality' => $quality,
                'stockType' => $stockType,
                'location' => $location
            ]
        ]);
        $this->expectExceptionIfNegativeQuantity($quantity);

        return StockLogQuantityClient::put(
            $stockLogCollection,
            $this->clientConfig
        );
    }

    /**
     * @param int $quantity
     */
    protected function expectExceptionIfNegativeQuantity($quantity)
    {
        if ($quantity < 0) {
            $this->expectException(ClientException::class);
        }
    }
}