<?php
namespace StockClientTest\StockLog;

use Fig\Http\Message\StatusCodeInterface;
use StockClient\StockLog\StockLogClient;
use StockClient\StockLog\StockLogCollection;
use StockClientTest\AbstractClientTest;
use StockClientTest\PositiveCases;

class StockLogClientShopTest extends AbstractClientTest
{
    use PutStock;

    public function setUp()
    {
        parent::setUp();
        $this->clientConfig[self::AUTH_TOKEN] = $this->getShopToken();
    }

    /**
     * @dataProvider \StockClientTest\PositiveCases::positiveCasesShop()
     * @param int $quantity
     * @param int $quality
     * @param string $stockType
     * @param null|string $location
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testPutAllCombinationsWorks($quantity, $quality, $stockType, $location = null)
    {
        $this->putToStock($quantity, $quality, $stockType, $location);
    }
}