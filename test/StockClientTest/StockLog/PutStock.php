<?php
namespace StockClientTest\StockLog;

use Fig\Http\Message\StatusCodeInterface;
use StockClient\StockLog\StockLogClient;
use StockClient\StockLog\StockLogCollection;

trait PutStock
{
    /**
     * @param int $quantity
     * @param int $quality
     * @param string $stockType
     * @param null|string $location
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function putToStock($quantity, $quality, $stockType, $location = null)
    {
        $stockLogCollection = StockLogCollection::create([
            [
                'sku' => self::SKU,
                'quantity' => $quantity,
                'quality' => $quality,
                'stockType' => $stockType,
                'location' => $location
            ]
        ]);

        $result = StockLogClient::put(
            $stockLogCollection,
            $this->clientConfig
        );

        $this->assertEquals(StatusCodeInterface::STATUS_NO_CONTENT, $result->getStatusCode());
    }
}