<?php
namespace StockClientTest\StockSearch;

use Fig\Http\Message\StatusCodeInterface;
use StockClientTest\AbstractClientTest;
use StockClientTest\PositiveCases;

class StockSearchClientTest extends AbstractClientTest
{
    public function testLocationsReturnsOnlySearchedValue()
    {
        foreach (PositiveCases::getAvailableLocationCodes() as $availableLocationCode) {
            $result = $this->sendSearchMessage([
                PositiveCases::LOCATION_NAME => [$availableLocationCode]
            ]);

            $this->assertEquals(StatusCodeInterface::STATUS_OK, $result->getStatusCode());

            foreach ($this->processResult($result) as $item) {
                $this->assertEquals($availableLocationCode, $item->location);
            }
        }
    }

    public function testQualitiesReturnsOnlySearchedValue()
    {
        foreach (PositiveCases::getAvailableQualities() as $availableQuality) {
            $result = $this->sendSearchMessage(
                [
                    PositiveCases::QUALITY_NAME => [$availableQuality]
                ]
            );

            $this->assertEquals(StatusCodeInterface::STATUS_OK, $result->getStatusCode());

            foreach ($this->processResult($result) as $item) {
                $this->assertEquals($availableQuality, $item->quality);
            }
        }
    }

    public function testStockTypeReturnsOnlySearchedValue()
    {
        foreach (PositiveCases::getAvailableTypes() as $availableType) {
            $result = $this->sendSearchMessage(
                [
                    PositiveCases::STOCK_TYPE_NAME => [$availableType]
                ]
            );

            $this->assertEquals(StatusCodeInterface::STATUS_OK, $result->getStatusCode());

            foreach ($this->processResult($result) as $item) {
                $this->assertEquals($availableType, $item->stockType);
            }
        }
    }
}
