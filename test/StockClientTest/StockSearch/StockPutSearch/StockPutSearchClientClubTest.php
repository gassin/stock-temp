<?php
namespace StockClientTest\StockSearch\StockPutSearch;

use StockClient\StockLog\StockLogClient;
use StockClient\StockLog\StockLogCollection;
use StockClientTest\AbstractClientTest;

class StockPutSearchClientClubTest extends AbstractClientTest
{
    use StockPutSearch;

    public function setUp()
    {
        parent::setUp();
        $this->clientConfig[self::AUTH_TOKEN] = $this->getClubToken();
    }

    /**
     * @param int $quantity
     * @param int $quality
     * @param string $stockType
     * @param null|string $location
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @dataProvider \StockClientTest\PositiveCases::positiveCasesClub()
     */
    public function testItPutsToStockAndCompare($quantity, $quality, $stockType, $location = null)
    {
        $initialSearchObject = $this->putStockSearchStockMakeAssertionsAndGetSearchResult(
            $quantity,
            $quality,
            $stockType,
            $location
        );
        $secondSearchObject = $this->putStockSearchStockMakeAssertionsAndGetSearchResult(
            $quantity,
            $quality,
            $stockType,
            $location
        );
        $this->assertEquals($secondSearchObject->quantity, $initialSearchObject->quantity + $quantity);
    }
}
