<?php
namespace StockDomainTest\ValueObjects\Location;

use PHPUnit\Framework\TestCase;
use StockDomain\ValueObjects\Location\LocationNotFoundException;
use StockDomain\ValueObjects\Location\LocationVO;

class LocationVOTest extends TestCase
{
    /**
     * @dataProvider getPositiveCasesDataProvider
     * @param int|null $location
     * @throws \StockDomain\ValueObjects\Location\LocationNotFoundException
     */
    public function testLocationVOIsCreated(?int $location)
    {
        $locationVO = LocationVO::create($location);
        $this->assertInstanceOf(LocationVO::class, $locationVO);
        $this->assertEquals($location, $locationVO->get());
        $this->assertEquals($locationVO->__toString(), (string) $locationVO->get());
        $this->assertEquals($locationVO->jsonSerialize(), (string) $locationVO->get());
    }

    /**
     * @throws LocationNotFoundException
     */
    public function testLocationIsNotCreated()
    {
        $this->expectException(LocationNotFoundException::class);
        new LocationVO(1);
    }

    /**
     * @return array
     */
    public function getPositiveCasesDataProvider() : array
    {
        $dataProvider[] = [null];
        /** @var array $item */
        foreach (LocationVO::getAvailableLocationCodes() as $location) {
            $dataProvider[] = [$location];
        }
        return $dataProvider;
    }
}