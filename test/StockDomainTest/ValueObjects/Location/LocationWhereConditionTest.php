<?php
namespace StockDomainTest\ValueObjects\Location;

use PHPUnit\Framework\TestCase;
use StockDomain\Repository\SearchStockRepository\SearchStockRepository;
use StockDomain\ValueObjects\Location\LocationVO;
use StockDomain\ValueObjects\Location\LocationWhereCondition;

class LocationWhereConditionTest extends TestCase
{
    /**
     * @throws \StockDomain\ValueObjects\Location\LocationNotFoundException
     */
    public function testLocationWhereConditionIsCreated()
    {
        $locations = LocationVO::getAvailableLocationCodes();
        $locationConditions = [];
        $locationsWhereConditions = [];

        /** @var int $location */
        foreach ($locations as $location) {
            $locationConditions[LocationVO::NAME][] = new LocationVO($location);
        }

        $locationWhereCondition = new LocationWhereCondition($locationConditions);
        $this->assertEquals(count($locations), count($locationWhereCondition->getParams()));

        foreach ($locationConditions[LocationVO::NAME] as $key => $condition) {
            $paramName = ':'.LocationVO::NAME.'param'.$key;
            $locationsWhereConditions[$paramName] = SearchStockRepository::TABLE_ALIAS.
                '.'.LocationVO::NAME.' = '.$paramName;
            $whereParams[$paramName] = $condition->get();
        }

        $this->assertEquals($locationWhereCondition->getWhere(), implode(' or ', $locationsWhereConditions));
    }

    public function getDataProvider()
    {
        $dataProvider = [];
        /** @var array $item */
        foreach (LocationVO::getAvailableLocationCodes() as $location) {
            $dataProvider[] = [$location];
        }
        return $dataProvider;
    }
}