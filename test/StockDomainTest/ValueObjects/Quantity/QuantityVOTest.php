<?php
namespace StockDomainTest\ValueObjects\Quantity;

use PHPUnit\Framework\TestCase;
use StockDomain\ValueObjects\Quantity\QuantityVO;
use StockDomain\ValueObjects\Quantity\QuantityZeroException;

class QuantityVOTest extends TestCase
{
    /**
     * @dataProvider getPositiveCasesDataProvider
     * @param int $quantity
     * @throws \StockDomain\ValueObjects\Quantity\QuantityZeroException
     */
    public function testQuantityVOIsCreated(int $quantity)
    {
        $quantityVO = new QuantityVO($quantity);
        $this->assertInstanceOf(QuantityVO::class, $quantityVO);
        $this->assertEquals($quantityVO, $quantityVO->__toString());
        $this->assertEquals($quantityVO->__toString(), (string) $quantityVO->get());
        $this->assertEquals($quantityVO->jsonSerialize(), (string) $quantityVO->get());
    }

    /**
     * @return array
     */
    public function getPositiveCasesDataProvider() : array
    {
        return [
            'positive' => [1],
            'negative' => [-1]
        ];
    }
}