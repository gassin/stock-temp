<?php
namespace StockDomainTest\ValueObjects\StockType;

use PHPUnit\Framework\TestCase;
use StockDomain\ValueObjects\StockType\InvalidStockTypeException;
use StockDomain\ValueObjects\StockType\StockTypeVO;

class StockTypeVOTest extends TestCase
{
    /**
     * @dataProvider getPositiveCasesDataProvider
     * @param $stockType
     * @throws \StockDomain\ValueObjects\StockType\InvalidStockTypeException
     */
    public function testStockTypeVOIsCreated($stockType)
    {
        $stockTypeVO = new StockTypeVO($stockType);
        $this->assertInstanceOf(StockTypeVO::class, $stockTypeVO);
        $this->assertEquals($stockType, $stockTypeVO->__toString());
    }

    /**
     * @throws InvalidStockTypeException
     */
    public function testStockTypeIsNotCreated()
    {
        $this->expectException(InvalidStockTypeException::class);
        new StockTypeVO('invalidstocktype');
    }

    /**
     * @return array
     */
    public function getPositiveCasesDataProvider() : array
    {
        $dataProvider = [];
        foreach (StockTypeVO::getAvailableTypes() as $availableType) {
            $dataProvider[] = [$availableType];
        }
        return $dataProvider;
    }
}
