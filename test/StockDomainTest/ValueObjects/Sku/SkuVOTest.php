<?php
namespace StockDomainTest\ValueObjects\Sku;

use PHPUnit\Framework\TestCase;
use StockDomain\ValueObjects\Sku\InvalidSkuException;
use StockDomain\ValueObjects\Sku\SkuVO;

class SkuVOTest extends TestCase
{
    public function testSkuVOIsCreated()
    {
        $skuVO = SkuVO::create('RUTEST-SKU');
        $this->assertInstanceOf(SkuVO::class, $skuVO);
        $this->assertTrue($skuVO->equals(SkuVO::create('RUTEST-SKU')));
        $this->assertFalse($skuVO->equals(SkuVO::create('RUTEST')));
        $this->assertEquals($skuVO, $skuVO->__toString());
        $this->assertEquals($skuVO->__toString(), $skuVO->jsonSerialize());
    }

    public function testSkuVOIsNotCreated()
    {
        $this->expectException(InvalidSkuException::class);
        new SkuVO('');
    }
}
