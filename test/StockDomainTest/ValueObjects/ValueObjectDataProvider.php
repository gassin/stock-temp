<?php
declare(strict_types = 1);

namespace StockDomainTest\ValueObjects;

use StockDomain\ValueObjects\Location\LocationVO;
use StockDomain\ValueObjects\Quality\QualityVO;
use StockDomain\ValueObjects\Source\SourceVO;
use StockDomain\ValueObjects\StockType\StockTypeVO;
use StockDomain\ValueObjects\User\UserVO;
use Exception;

class ValueObjectDataProvider
{
    const USER_FOR_TEST = 'user.for.test';

    /**
     * @return array
     * @throws Exception
     */
    public static function positiveCasesClub() : array
    {
        $result = [];

        foreach (LocationVO::getAvailableLocationCodes() as $location) {
            foreach (QualityVO::getAvailableQualities() as $quality) {
                foreach ([1, -1] as $quantity) {
                    foreach (self::getAvailableTypesBySource(new SourceVO(SourceVO::CLUB)) as $stockType) {
                        $result[] = [$quantity, $quality, $stockType->__tostring(), $location];
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @return array
     * @throws Exception
     */
    public static function positiveCasesShop() : array
    {
        $result = [];

        foreach (LocationVO::getAvailableLocationCodes() as $location) {
            foreach (QualityVO::getAvailableQualities() as $quality) {
                foreach ([1, -1] as $quantity) {
                    /** @var StockTypeVO $stockType */
                    foreach (self::getAvailableTypesBySource(new SourceVO(SourceVO::SHOP)) as $stockType) {
                        $result[] = [$quantity, $quality, $stockType->__tostring(), $location];
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @return array
     * @throws Exception
     */
    public static function positiveCasesDelta() : array
    {
        $result = [];

        foreach (LocationVO::getAvailableLocationCodes() as $location) {
            foreach (QualityVO::getAvailableQualities() as $quality) {
                foreach ([1, -1] as $quantity) {
                    foreach (self::getAvailableTypesBySource(new SourceVO(SourceVO::DELTA)) as $stockType) {
                        $result[] = [$quantity, $quality, $stockType->__tostring(), $location];
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @return array
     * @throws Exception
     */
    public static function getSourceStockTypesCombinations() : array
    {
        $result = [];
        foreach (self::getAvailableSources() as $availableSource) {
                $result[] = self::getAvailableTypesBySource($availableSource);
        }
        return $result;
    }

    /**
     * @return array
     */
    public static function getAvailableSources() : array
    {
        return [
            SourceVO::create(SourceVO::CLUB),
            SourceVO::create(SourceVO::SHOP),
            SourceVO::create(SourceVO::DELTA)
        ];
    }

    /**
     * @param SourceVO $source
     * @return array
     * @throws \Exception
     */
    public static function getAvailableTypesBySource(SourceVO $source) : array
    {
        if ($source->isClub()) {
            return [
                StockTypeVO::create(StockTypeVO::VIRTUAL_CLUB),
                StockTypeVO::create(StockTypeVO::PHYSICAL_FREE)
            ];
        } elseif ($source->isShop()) {
            return [
                StockTypeVO::create(StockTypeVO::VIRTUAL_SHOP),
                StockTypeVO::create(StockTypeVO::PHYSICAL_FREE)
            ];
        } elseif ($source->isDelta()) {
            return [
                StockTypeVO::create(StockTypeVO::VIRTUAL_SHOP),
                StockTypeVO::create(StockTypeVO::VIRTUAL_CLUB),
                StockTypeVO::create(StockTypeVO::PHYSICAL_FREE),
            ];
        }
    }

    /**
     * @return UserVO
     */
    public static function getTestUser() : UserVO
    {
        return new UserVO(self::USER_FOR_TEST);
    }
}