<?php
namespace StockDomainTest\ValueObjects\Quantity;

use PHPUnit\Framework\TestCase;
use StockDomain\ValueObjects\Quality\QualityNotFoundException;
use StockDomain\ValueObjects\Quality\QualityVO;

class QualityVOTest extends TestCase
{

    /**
     * @param int $quality
     * @dataProvider getPositiveCasesDataProvider
     * @throws \StockDomain\ValueObjects\Quality\QualityNotFoundException
     */
    public function testQualityVOIsCreated(int $quality)
    {
        $qualityVO = QualityVO::create($quality);
        $this->assertInstanceOf(QualityVO::class, $qualityVO);
        $this->assertEquals($qualityVO, $qualityVO->__toString());
        $this->assertEquals($qualityVO->__toString(), (string) $qualityVO->get());
        $this->assertEquals($qualityVO->jsonSerialize(), (string) $qualityVO->get());
    }

    public function testQualityVOIsNotCreated()
    {
        $this->expectException(QualityNotFoundException::class);
        new QualityVO(1000);
    }

    /**
     * @return array
     */
    public function getPositiveCasesDataProvider() : array
    {
        $dataProvider = [];
        foreach (QualityVO::getAvailableQualities() as $qualityCode) {
            $dataProvider[] = [$qualityCode];
        }
        return $dataProvider;
    }
}